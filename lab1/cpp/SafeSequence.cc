#include "SafeSequence.h"

SafeSequence::SafeSequence()
    :value(0){}

int SafeSequence::getNext() {
    mtx.lock();
    int tmp = value++;  // Increment under mutex protection
    mtx.unlock();
  return tmp;
}
