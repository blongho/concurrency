#include "SafeSequenceTest.h"
#include <iostream>
using std::cout;
using std::endl;

SafeSequenceTest::SafeSequenceTest(int limit, int nThreads)
    :MAX(limit), THREADS(nThreads)
    {
        // Create the wanted number of sequence threads
        for(int i=0;i<THREADS;++i)
            threads.push_back(thread());  // default construct for deferred start, does not yet represent a thread of execution

        // Start the threads
        for(int i=0;i<THREADS;++i) {
            threads[i] = thread(SequenceThreadFunctor(safe, values, MAX, mtx)); // Start the thread
        }

        // Wait for the threads to finish
        for(int i=0;i<THREADS;++i)
            threads[i].join();

        // Check for errors

        bool errorsFound { false };
        int prevValue { values[0] };
        int nErrors {0};

        for (int i = 1; i< values.size(); ++i){
            if (values[i] <= prevValue) {
                errorsFound = true;
                break;
            }
            prevValue = values[i];
        }

        if(errorsFound) {
            for (int i = 0; i< values.size(); ++i)
                if (i != values[i]) {
                    cout << "Expected " << i << " got " << values[i] << endl;
                    ++nErrors;
            }
            cout << "\n" << nErrors << " numbers were reordered\n" << endl;
        }
        else
            cout << "Success!\nAll values were stored in correct increasing order.\n" << endl;
    }
