#!/bin/sh
u=$USERNAME
n=0
file="lab1"
if [ ! -e $file ]
then
    echo "File $file does not exist."
    echo "Should bash make the file for you [y/n]"
    answer="n"
    read answer
    if [ $answer = 'y' ]
    then
        echo "*****Ok, running 'make all'*****\n"
        make all
        echo "\n*****Program ready*****\n"

        echo "Hello, $u! How many times do you want to run the program?: "
        read n

        clear

        # TODO
        # check the input before running the iterations

        for i in $(seq $n)
        do
            echo "Run #$i.."
            lab1
            echo "========================"
        done
    else
        echo "Ok, Next time $u"
    fi
else
    echo "Hello, $u! How many times do you want to run the program?: "
    read n

    clear


    for i in $(seq $n)
    do
        echo "Run #$i.."
        lab1
        echo "========================"
    done
fi
