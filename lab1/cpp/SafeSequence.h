#ifndef SAFESEQUENCE_H
#define SAFESEQUENCE_H
#include <mutex>
using std::mutex;

class SafeSequence {
private:
    int value;
    mutex mtx;  // To ensure serialized access to value

public:
    SafeSequence();

    /**
     * Returns next value in sequence.
     */
    int getNext();
};


#endif // SAFESEQUENCE_H
