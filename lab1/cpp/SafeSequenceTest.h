#ifndef SAFESEQUENCETEST_H
#define SAFESEQUENCETEST_H
#include "SequenceThreadFunctor.h"
#include <thread>
using std::thread;

class SafeSequenceTest {

private:
    const int MAX;  // Sequence upper limit
    const int THREADS; // # threads

    // The threads
    vector<thread> threads;

    // A vector for the produced numbers, shared among the threads
    vector<int> values;

    // The source of the sequence 0,1,2...
    SafeSequence safe;  // Shared among the threads

    // mutex to prevent concurrent access to vector::push_back()
    mutex mtx;

public:
    SafeSequenceTest(int limit, int nThreads);
};

#endif // SAFESEQUENCETEST_H
