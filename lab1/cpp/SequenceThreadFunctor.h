#ifndef SEQUENCETHREADFUNCTOR_H
#define SEQUENCETHREADFUNCTOR_H
#include "SafeSequence.h"
#include <vector>
using std::vector;

class SequenceThreadFunctor {
private:
    SafeSequence &iSeq; // Reference to shared UnsafeSequence
    vector<int> &iVec; // Reference to shared vector for number storage
    const int iLimit;
    mutex &mutexRef;  // To ensure serialized access to value

public:
    SequenceThreadFunctor(SafeSequence &seq, vector<int> &vec, int limit, mutex &mtx);


    void operator()();
};

#endif // SEQUENCETHREADFUNCTOR_H
