TEMPLATE = app
CONFIG += console c++11
CONFIG += thread
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    SafeSequence.cc \
    SequenceThreadFunctor.cc \
    SafeSequenceTest.cc \
    Main.cc

HEADERS += \
    SafeSequence.h \
    SequenceThreadFunctor.h \
    SafeSequenceTest.h
