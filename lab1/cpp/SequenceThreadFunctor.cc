#include "SequenceThreadFunctor.h"

SequenceThreadFunctor::SequenceThreadFunctor(SafeSequence &seq,
                                             vector<int> &vec, int limit,
                                             mutex &mtx)
    :iSeq(seq),iVec(vec),iLimit(limit), mutexRef(mtx) {}

void SequenceThreadFunctor::operator()()
{
    int n=0;
    // Get numbers and add them to the shared Vector
    while(true) {
        n = iSeq.getNext(); // this line should come below mutexRef.lock() for correct sequence generation
        mutexRef.lock();
        if(n<=iLimit) {
            iVec.push_back(n); // Synchronize push_backs
            mutexRef.unlock();
        }
        else {
            mutexRef.unlock();
            break;
        }
    }
}
