When the program code sequence of
```cpp
mutexRef.lock()
n = iSeq.getNext();
```
is changed to

```cpp
mutexRef.lock()
n = iSeq.getNext();
```
the results are unpredictable. For some runs, the program program works
as expected (no sequence errors) and some the sequence errors.

This is because there is a race condition resulting in incorrect access
to the shared resource (iVec)
