#ifndef CLIENT_H
#define CLIENT_H
#include "TCPsocket.h"

class Client
{
private:
    const static int SIZE = 1024;
    string host;
    u_port_t port;
    int communfd;
    TCPsocket socket;
    void connect();
public:
    Client();
    Client(const string &hostname, u_port_t uport);
    ~Client();
    void sendMessage(std::string &msg);
    void receiveMessage(std::string &msg);
    void close();
    bool isClose() const;
};

#endif // CLIENT_H
