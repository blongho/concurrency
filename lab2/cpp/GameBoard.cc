/**
  * @file   Board.cc
  * @brief  Implementation of Board.h
  * @since  2018-10-25
  * */
#include "GameBoard.h"
#include <iomanip>
using std::setw;

void GameBoard::reset()
{
    for(int row = 0; row < ROWS; row++){
        for(int col = 0; col < COLUMNS; col++){
            data[row][col] = EMPTY;
        }
    }
}

GameBoard::GameBoard()
{
    reset();
}


void GameBoard::insert(const Point &p, const char &val)
{
    data[p.y][p.x] = val;
}

void GameBoard::display(std::ostream &os)
{
    os << "-----------------------------------------------------\n";
    os << setw(2) << ' ';
    for(int col = 0; col < COLUMNS; col++){
        os << setw(4) << col;
    }
    os << '\n';

    for(int row = 0; row < ROWS; row++){
        os << setw(2) << row; // print the row numbers
        for(int col = 0; col < COLUMNS; col++){
            os << setw(4) << data[row][col];
        }
        os << '\n';
    }
    os << "-----------------------------------------------------\n";
}
