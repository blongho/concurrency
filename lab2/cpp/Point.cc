#include "Point.h"

Point::Point(int row, int column)
    :x(row), y(column){}


std::ostream &operator<<(std::ostream&os, const Point&p){
    return os << "Point{" << p.x << ", " << p.y << "}";
}

bool isValidPoint(const Point &point)
{
    return point.x != INVALID_CORD && point.y != INVALID_CORD;
}
