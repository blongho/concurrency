/**
  * @file   TestProgram.h
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @since  2018-11-18
  *
  * @brief  The test program for lab2
  * After complilation run lab2
  * */
#ifndef TESTPROGRAM_H
#define TESTPROGRAM_H
#include <atomic>
#include <mutex>
#include <thread>
#include "Client.h"
#include "GameBoard.h"


class TestProgram
{
private:
    const in_port_t PORT = 6501; // the tcp connection port
    const std::string HOST = "luffarserv.miun.se"; // the tcp host server addr
    const char MARKER_HUMAN = 'X'; // marker on the board for human moves
    const char MARKER_AI = 'O'; // marker on the for AI moves
    Client client;
    GameBoard board; // the game board
    std::atomic_bool &playing;
    std::string winner; // the winner of the game
    std::string player;  // the name of the player
    std::string opponent; // the opponent {Striker | Defender}
    int starter; // Get the starter {0=ai, 1=human} randomly generated
    std::recursive_mutex &mutexLock; // the lock to ensure mutual exclusion
    Point aiLastPoint;
    Point playerLastPoint;
    /**
     * @brief initGame Initialize the game
     * Here the user
     * - enters the s(he) name
     * - chooses an opponent
     * - the function beginner() is called that chooses the beginner
     * - the user sends the request to start a game to the server
     */
    void initGame();
    /**
     * @brief getPoint Process a MOV:x:y command and get the values of x and y
     * @param msg the move command passed
     * @return a Point(x, y)
     */

    /**
     * @brief decideStarter decide who makes the first move.
     * @return randomly generated value 0 for human and 1 for ai
     */
    int decideStarter();

    /**
     * @brief isHumanMove check if the starter is human or not
     * @return returns true $starter == 0
     */
    bool isHumanStart();


    /**
     * @brief getPoint Process the information in msg and extract the x and y
     * @param msg a message to be processed. Typically MOV:00:00
     * @return {x,y} or {-1,-1}
     */
    Point getPoint(std::string &msg);

    /**
     * @brief processHumanMove quit the game if move==QUI, otherwise the point
     * from the move and insert that into the board.
     *
     * Send the message to the server
     * @param move the human move obtained from humanPlay()
     */
    void processHumanMove(std::string &move);

    /**
     * @brief processAiResponse Process the information from the server and
     * print the appropriate response. If response contains
     * - ILC -> illegal command. End game
     * - ILM -> illegal move. End game
     * - WIN -> win. Print winner. End game
     * - TCL -> time out. End game
     * - NAP -> no player set. End game
     * - MOV -> getPoint and insert into board
     * @param response the response string from the server
     */
    void processAiResponse(std::string &response);

    void setOpponent(int value);

    void clearScreen(); // auxilliary function to clear the console

    /**
     * @brief doHumanChat get a chat message and format it for the server
     * @return CHA:nn:msg\n
     */
    //std::string doHumanChat();

    /**
     * @brief doHumanMove Get a move from the user
     * @return MOV:xx:yy
     */
    std::string doHumanMove();

    /**
     * @brief getNumber get a number from the user. Use this to get any
     *  positive number value.
     * @return returns -1 or a number > -1
     * @throws invalid_argument exception if user enters a value that
     * std::stoi can not convert to a string
     */
    int getNumber() const;

    /**
     * @brief sendMessage Sends msg via the client object to the server
     * @param msg the message to be sent
     */
    void sendMessage(std::string &msg);

    /**
     * @brief showBoard Prints the game board on screen
     */
    void showBoard();
    /**
     * @brief insertPoint insert a value into the game board
     * @param p point (xx,yy)
     * @param marker 'X' for human and 'O' for AI as declared above
     */
    void insertPoint(Point &p, char marker);

    /**
     * @brief makeAiMove extract values of AI point and insert into game board
     * @param response the AI response from the server
     */
    void makeAiMove(std::string &response);


    /**
     * @brief isPlaying Checks whether the atomic bool flag of playing is set
     * @return playing.load() value
     */
    bool isPlaying();
    void endGame(); // close the socket and end the game

    /**
     * @brief receiveResponse Receive a response from the server
     * @param res the response received
     */
    void receiveResponse(std::string &res);

    /**
     * @brief chatWithAi make a one-on-one chat with the AI
     * @param msg the response string from the AI
     */
    void showAiMessage(std::string &msg);

    /**
      * @brief run Runs the program
      * Starts by initiating a connection to the server with a CHA message and
      * then processes the subsequent responses as they come from the server
      */
    void run();

public:
    /**
     * @brief TestProgram Initializes and oppens the client
     * initializes the board
     * If client connection fails, a runtime error is thrown
     */
    TestProgram(std::recursive_mutex &mtx, std::atomic_bool &play);

    void operator()();
};
#endif // TESTPROGRAM_H
