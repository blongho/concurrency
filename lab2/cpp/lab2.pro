TEMPLATE = app
CONFIG += console c++11
CONFIG += thread
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += -lnsl

SOURCES += \
    Restart.cc \
    uici.cc \
    uiciname.cc \
    TCPsocket.cc \
    Main.cc \
    Client.cc \
    TestProgram.cc \
    GameBoard.cc \
    Point.cc

HEADERS += \
    Restart.h \
    uici.h \
    uiciname.h \
    TCPsocket.h \
    Client.h \
    TestProgram.h \
    GameBoard.h \
    Point.h
