/**
  * @file   Board.h
  * @author Bernard Che Longho (lobe1602)
  * @brief  The Game playground (a 15 * 15 two dimensional array)
  *
  * @since  2018-10-25
  * */
#ifndef GAME_BOARD_H
#define GAME_BOARD_H
#include "Point.h"
#include <iostream>
#include <string>

class GameBoard
{
private:
    const static int ROWS = 15;     /** The rows of the board*/
    const static int COLUMNS = 15;  /** The columns of the board*/
    const static char EMPTY = '-';  /** A char denoting that location is empty*/
    char data[ROWS][COLUMNS];       /** The container holder the game moves */
    void reset();   /** Resets any character on the board with #EMPTY */
public:
    /**
      The default constructor starts and resets the board to all empty
     * @brief Board
     */
    GameBoard();
    ~GameBoard() = default;

    /**
     * @brief insert Inset an item into the board
     * @param val The value to be inserted
     */
    void insert(const Point&, const char &val);
    /**
     * @brief display Show the information contained in #data
     * @param os the output stream
     */
    void display(std::ostream &os=std::cout);
};
#endif // BOARD_H
