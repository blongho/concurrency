#include "TestProgram.h"
#include <exception>
#include <random>
#include <regex>
#include <ctime>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::getline;

// program will end if connection cannot be established
TestProgram::TestProgram(std::recursive_mutex &mtx, std::atomic_bool &play)
    : playing(play), mutexLock(mtx) {
    try {
        client = Client(HOST, PORT);

    } catch (std::runtime_error &re) {
        throw std::runtime_error(re.what());
    }
    board = GameBoard();
}

void TestProgram::operator()() {
    run();
}

void TestProgram::initGame() {
    clearScreen();

    cout << "\nWelcome to the tic-tac-to deluxe from luffarserv.miun.se\n" << endl;
    cout << "To start, enter your name: ";
    getline(cin, player);

    int opp{-1}; // opponent

    do {
        cout << "Choose an opponent [0=Striker, 1=Defender]: ";
        try {
            opp = getNumber();
        } catch (std::invalid_argument &e) {
            cerr << e.what() << endl;
        }
    } while (opp < 0 || opp > 1);

    setOpponent(opp);

    starter = decideStarter(); // set starter 0 = human and 1 = ai

    string playerSize = player.size() < 10 ? "0" + std::to_string(player.size())
                                           : std::to_string(player.size());

    string initmsg = "CHA:" + std::to_string(opp) + ":" +
            std::to_string(starter) + ":" + playerSize + ":" + player +
            "\n";

    sendMessage(initmsg);
}

int TestProgram::decideStarter() {
    // Random generate who starts
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<int> distribution(0, 1);
    return distribution(generator);
}

Point TestProgram::getPoint(std::string &msg) {
    // std::string mov("MOV:00:00");
    Point point;

    if (!msg.empty()) { // this is a move command
        std::string numbers = msg.substr(msg.find_first_of(':') + 1);
        std::string xvalue = numbers.substr(0, numbers.find(':'));
        std::string yvalue = numbers.substr(numbers.rfind(':') + 1);
        int x{INVALID_CORD}, y{INVALID_CORD};
        try {
            x = std::stoi(xvalue);
        } catch (std::invalid_argument &) {
            const std::string error = "Your x-value is wrong format ["
                    + xvalue + "]";
            throw std::invalid_argument(error.c_str());
        }
        try {
            y = std::stoi(yvalue);
        } catch (std::invalid_argument) {
            const std::string error = "Your y-value is wrong format ["
                    + yvalue + "]";
            throw std::invalid_argument(error.c_str());
        }

        point = Point(x, y);
    }
    return point;
}

void TestProgram::processHumanMove(std::string &move) {
    if (move == "QUI") {
        playing.store(false);
    } else if (move.find("CH") != std::string::npos) {
        //cout << move.substr(move.find_last_of(':') + 1) << endl;
        sendMessage(move);
    } else if (move.find("MOV") != std::string::npos) {
        clearScreen();
        //showBoard();
        Point hpoint;
        try {
            hpoint = getPoint(move);
        } catch (...) {
            cout << "[ProcessHumanMove()] error"<< endl;
        }
        if(isValidPoint(hpoint)){ // bad point
            insertPoint(hpoint, MARKER_HUMAN);
            playerLastPoint = hpoint;
            showBoard();
            sendMessage(move);
        }
    }
}

void TestProgram::processAiResponse(std::string &response) {
    if(!response.empty()){

        if (response.find("ILC") != std::string::npos) {
            playing.store(false);
            cerr << "Illegal command received [" << response << "]" << endl;
        } else if (response.find("ILM") != std::string::npos) {
            playing.store(false);
            cerr << "Illegal move [" << response << "]" << endl;

        } else if (response.find("WIN") != std::string::npos) {
            if (response.find("MOV") != std::string::npos) {
                winner = opponent;
            } else {
                winner = player;
            }
            cout << winner << " wins (" << response << ")!" << endl;
            playing.store(false);
        } else if (response.find("TCL") != std::string::npos) {
            playing.store(false);
            cerr << "I don't have time to waste. Game over!" << endl;
        } else if (response.find("NAP") != std::string::npos) {
            playing.store(false);
            cerr << "No player set. Game over." << endl;
        } else if (response.find("MOV") != std::string::npos) {
            makeAiMove(response);
        } else if (response.find("CHT") != std::string::npos) {
            showAiMessage(response);
        }
    }
}

void TestProgram::setOpponent(int value) {
    if (value == 0) {
        opponent = "Striker";
    } else if (value == 1) {
        opponent = "Defender";
    }
}

std::string TestProgram::doHumanMove(){
    clearScreen();
    showBoard();
    time_t now = time(nullptr);
    tm *gmt = gmtime(&now);
    int hour = gmt->tm_hour;
    string greeting;
    if(hour < 12){
        greeting = "Good morning ";
    }else if(hour > 12 && hour < 15){
        greeting = "Good afternoon ";
    }else{
        greeting = "Good evening ";
    }

    greeting += player;
    cout <<"\n\n" << greeting << "!\n\nTo make a move enter 'xx, xx'"
        <<"(Your moves are marked with '"<< MARKER_HUMAN << "')\nTo quit, enter"
       << " 'quit'\nAny other string to chat \n\nYour input:->  ";

    string input;

    getline(cin, input);

    const size_t cmdSize = input.size();
    const string length =
            cmdSize < 10 ? "0" + std::to_string(cmdSize) : std::to_string(cmdSize);

    if(input == "quit"){
        return "QUI";
    }
    else if (input.find(',') != std::string::npos) { // no intentions to insert
        const size_t commaPos = input.find(',');
        string xvalue = input.substr(0, commaPos);
        string yvalue = input.substr(commaPos + 1);

        // clear trailing spaces i.e return '04' if value is ' 04 '
        xvalue = regex_replace(xvalue, std::regex(" +"), "");
        yvalue = regex_replace(yvalue, std::regex(" +"), "");
        xvalue = xvalue.size() < 2 ? "0" + xvalue : xvalue;
        yvalue = yvalue.size() < 2 ? "0" + yvalue : yvalue;

        // Add trailing new line to command
        return "MOV:" + xvalue + ":" + yvalue + "\n";
        // cout << cmd << endl;
    }else{
        return "CHT:" + length + ":" + input + "\n";

    }

}

void TestProgram::makeAiMove(std::string &response) {
    Point aipoint = getPoint(response);
    clearScreen();
    //cout << opponent << " move: " << getPoint(response) << endl;
    insertPoint(aipoint, MARKER_AI);
    aiLastPoint = aipoint;
    showBoard();
}


void TestProgram::run() {
    initGame();
    int rounds{};
    string aiMessage; // ai message
    receiveResponse(aiMessage);
    if (aiMessage.find("CHT") != std::string::npos){
        showAiMessage(aiMessage);
    }
    if (!aiMessage.empty() && aiMessage[0] == 'O') // this is ok message
    {
        clearScreen();
        playing.store(true);
        string gameResponse;
        std::string humanMove;
        while (isPlaying() && !client.isClose()) {
            if (isHumanStart()) {
                if (rounds < 1) {
                    cout << "You[" << player << "] start" << endl;
                }
                mutexLock.lock();

                humanMove = doHumanMove();
                processHumanMove(humanMove);
                mutexLock.unlock();
                mutexLock.lock();
                receiveResponse(gameResponse);
                processAiResponse(gameResponse);
                mutexLock.unlock();
            } else {
                if (rounds < 1) {
                    cout << opponent << " starts" << endl;
                }
                mutexLock.lock();
                receiveResponse(gameResponse);
                processAiResponse(gameResponse);
                mutexLock.unlock();
                mutexLock.lock();
                humanMove = doHumanMove();
                processHumanMove(humanMove);
                mutexLock.unlock();
            }
            rounds++;
        }
        endGame();
    } else {
        perror("Communication to the server could not be established");
        endGame();
    }
}


int TestProgram::getNumber() const {
    std::string choice;
    std::getline(std::cin, choice);

    int value{-1};
    try {
        value = std::stoi(choice);
    } catch (std::invalid_argument &) {
        const char * msg = "[Error]. Enter an integer\n";
        throw std::invalid_argument(msg);
    }
    return value;
}

void TestProgram::endGame() {
    playing.store(false);
    cout << "Quiting the program..." << endl;
    try {
        client.close();
    } catch (std::runtime_error &re) {
        cerr << re.what() << endl;
    }

}

bool TestProgram::isPlaying() { return playing.load(); }

bool TestProgram::isHumanStart() { return starter == 0; }

void TestProgram::sendMessage(std::string &msg) { client.sendMessage(msg); }

void TestProgram::receiveResponse(std::string &res) {
    client.receiveMessage(res);
}

void TestProgram::showAiMessage(std::string &msg) {
    cout << opponent << ": " << msg.substr(msg.find_last_of(':') + 1) << endl;

}

void TestProgram::showBoard()
{
     if(isValidPoint(aiLastPoint))
         cout << opponent << "'s last move: " << aiLastPoint << endl;
    board.display();
    if(isValidPoint(playerLastPoint))
        cout << player << "'s last point: " << playerLastPoint << endl;
}

void TestProgram::insertPoint(Point &p, char marker) {
    board.insert(p, marker);
}

void TestProgram::clearScreen() { system("clear"); }
