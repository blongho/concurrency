#ifndef POINT_H
#define POINT_H
#include <ostream>

static const int INVALID_CORD = -999;
class Point
{
public:
    int x{INVALID_CORD};
    int y{INVALID_CORD};

    Point() = default;
    Point(int row, int column);
};

bool isValidPoint(const Point &point);
std::ostream &operator<<(std::ostream&os, const Point&p);
#endif // POINT_H
