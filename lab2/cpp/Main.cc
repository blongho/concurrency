/**
  * @file    Main.cc
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @brief  Entry and exit point to the program of lab5
  * @since  2018-11-18
  *
  * Run:
  * lab2
  * */
#include "TestProgram.h"
#include <memstat.hpp>

int main()
{
    std::recursive_mutex mtx;
    std::atomic<bool> play{false};

    try {
        TestProgram test(mtx,play);
        std::thread game{test};

        try {
            if(game.joinable()){
                game.join();
            }

        } catch (std::system_error &se) { // join error
            perror(se.what());
        }
    } catch (std::runtime_error &re) { // connection errors
        perror(re.what());
        return 1;
    }

    return 0;
}


