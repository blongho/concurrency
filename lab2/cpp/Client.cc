#include "Client.h"
#include <iostream>
#include "Restart.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <cstdio>
#include <cstring>
#include <algorithm>


Client::Client()
    :host(""), port(0000), communfd(-1)
{}

Client::Client(const std::string &hostname, u_port_t uport)
    :host(hostname), port(uport), communfd(-1){
    connect();
}

Client::~Client()
{}

void Client::connect()
{
    try {
        socket.connect(host, port);
        communfd = socket.getDescriptor();
        std::cout << "Connection made to " << host << std::endl;
    } catch (std::runtime_error &re) {
        throw std::runtime_error(re.what());
    }
}

void Client::sendMessage(std::string &msg)
{
    char message[msg.size() + 1];
    strcpy(message, msg.c_str());
    if(r_write(communfd, message, sizeof(message)) == -1){
        perror("Failed to send message.");
    }
}

void Client::receiveMessage(std::string &msg)
{
    char feedback[SIZE];
    if(r_read(communfd, feedback, SIZE) == -1){
        perror("Failed to read message from server.");
    }
    const string message(feedback);
    memset(feedback, '\0', SIZE);
    msg = message.substr(0, message.size() - 2);
}

void Client::close()
{
    socket.close();
    communfd = -1;
}

bool Client::isClose() const
{
    return communfd == -1;
}

