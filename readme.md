# Programming with Concurrency and Parallelism
[Full description](https://www.miun.se/utbildning/kurser/Sok-kursplan/kursplan/?kursplanid=15141) (in swedish)

This course makes use of both c++ and java implementations of concurrency.

These will be specified in the respective module folders e.g
```
lab{n}      => root folder for the lab exercise, n = {1,2...8}
    cpp     => contains c++ implementation of lab{n}
    Java    => contains Java implementation of lab{n}
```

## System requirements
The programs will can be run in both Windows and Unix systems.
Where special considerations are required, these will be specified in the
laboratory's `readme` file.

However, the system wide requirements would be

- [QtCreator](https://www.qt.io/download) or [Visual Studio 2015+](https://visualstudio.microsoft.com/vs/older-downloads/)
- [Eclipse IDE for Java SE development](http://www.eclipse.org/downloads/packages/release/2018-09/r/eclipse-ide-java-developers)
- [Java 8 or more](https://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html)
- Compiler that can compile C++11

## Contribution guidelines
All work contained herein are my individual effort. You are however free
to make some suggestions for improvement. You can fork or clone this
project and create an issue or a pull request.

There is no guarantee that your suggestions will be incorporated into the
the main branch.

## Contact
For any enquiries please feel free to reach out at

[My school email](mailto:lobe1602@student.miun.se) (lobe1602[at]student.miun.se)

[My private email](mailto:blongho02@gmail.com) (blongho02[at]gmail.com)
