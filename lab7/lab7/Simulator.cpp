#include "Simulator.h"
#include <algorithm> // std::for_each
#include <thread>

Simulator::Simulator()
	:numberOfPhilosophers{ defaultNumOfPhilosophers },
	numOfPlates{ defaultNumOfPlates },
	logFileName{ defaultLogFile }{}

Simulator::Simulator(const int & nPhilosophers, const int & nEatingRounds, const string & logFile)
	: numberOfPhilosophers{ nPhilosophers }, numOfPlates{ nEatingRounds }, logFileName(logFile){}

void Simulator::setUpTable()
{
	// load the forks
	for (int i = 0; i < numberOfPhilosophers; i++) {
		forks.emplace_back(make_shared<Fork>(std::to_string(i + 1)));
	}

	// place the philosophers around the forks
	// make the shared log file
	ofs = make_shared<std::ofstream>(logFileName);

	const size_t forkSize = forks.size();

	for (int i = 0; i < numberOfPhilosophers; i++) {
		const string name{ "Philosopher"  + std::to_string(i +1)};
		philosophers.emplace_back(Philosopher(name, numOfPlates, *forks.at(i), *forks.at((i + 1) % forkSize), *ofs, numEating));
	}
}

void Simulator::eat()
{
	for (unsigned int i = 0; i < philosophers.size(); i++) {
		threads.emplace_back(thread(philosophers.at(i)));
	}

	// wait for the threads to finish
	std::for_each(threads.begin(), threads.end(), [](thread &t) {
		if (t.joinable()) {
			t.join();
		}
	});

	// close the file
	ofs->close();
}

void Simulator::run()
{
	setUpTable();
	eat();
}
