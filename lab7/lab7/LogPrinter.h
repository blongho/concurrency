/**
 * project	:	lab7
 * @file   	:	LogPrinter.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-28
 * @summary	:	Declares the log printer class
 * 				This class prints and saves the current state of the
 *				philosophers
 *				
 *				Copyright (c) 2018 Bernard Che Longho. 
 **/

#ifndef LOG_PRINTER_H
#define LOG_PRINTER_H

//#include "Philosopher.h"
#include <fstream>  // std::ofstream
#include <iostream> // std::cout, std::ostream
#include <mutex>
#include <string>

/** A log printer. */
class LogPrinter {
private:
  static std::mutex logMutex; /* The log mutex */
  /** Initializes a new instance of the LogPrinter class */
  LogPrinter()=default;

public:
  /** Finalizes an instance of the LogPrinter class */
  ~LogPrinter() = default;

  /**
   * Logs and prints the current state of the program
   *
   * @param [in,out]	ofs				The ofstream object.
   * @param [in,out]	philosopher		The philosopher's name.
   * @param [in,out]	os				The output stream.
   */

  static void logAndPrint(std::ofstream &ofs, const  std::string &information, std::ostream &os = std::cout);
};
#endif // !LOG_PRINTER_H

/**
// End of LogPrinter.h
 */
