#include "LogPrinter.h"

std::mutex LogPrinter::logMutex;

void LogPrinter::logAndPrint(std::ofstream & ofs, const std::string & information, std::ostream & os)
{
	std::lock_guard<std::mutex> lock(logMutex);
	ofs << information << '\n';
	os << information << '\n';
}
