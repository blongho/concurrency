/**
 * project	:	lab7
 * @file   	:	Helpers.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-28
 * @summary	:	Declares the helpers class
 * 				This class contains default values for the program
 * 				and some necessary global functions that does not
 * 				fit into any classes direct responsibility
 *
 *				Copyright (c) 2018 Bernard Che Longho.
 **/

#ifndef HELPERS_H
#define HELPERS_H

#include <random>

// Declare program constants
const int minSecs{ 1 };
const int maxSecs{ 5 };
const int defaultNumOfPhilosophers{ 6 };
const int defaultNumOfPlates{ 5 };
const std::string defaultLogFile{ "philo.log" };
const int minNumOfPhilosophers{ 4 };
const int maxNumOfPhilosophers{ 14 };
const int minNumOfPlates{ 4 };
const int maxNumOfPlates{ 10 };

/**
 * Generates a random integer
 *
 * @param	min	The minimum.
 * @param	max	The maximum.
 *
 * @return	The random integer.
 */

const int generateRandomInteger(const int &min, const int &max);

#endif // !HELPERS_H

/**
// End of Helpers.h
 */
