/**
 * project	:	lab7
 * @file   	:	Simulator.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-29
 * @summary	:	Declares the simulator class
 * 				This class does the running of the program
 * 				Variables needed for this class initialization are
 * 				-n The number of philosophers
 * 				-d the number of eating rounds for each philosopher
 * 				-f The file for logging all the changes during the program
 *
 **/

#ifndef SIMULATOR_H
#define SIMULATOR_H
#include "Philosopher.h"
#include <memory>
#include <thread>
#include <vector>

using std::make_shared; /* The standard make shared */
using std::shared_ptr;  /* The standard shared pointer */
using std::thread;  /* The standard thread */
using std::vector;  /* The standard vector */

/** A simulator. */
class Simulator {
private:
  int numberOfPhilosophers; /* Number of philosophers */
  int numOfPlates; /* The The number of plates each philosopher eats */
  string logFileName;   /* Filename of the log file */
  shared_ptr<std::ofstream> ofs;	/* The ofstream object for holding the shared logfile */
  vector<shared_ptr<Fork>> forks;   /* The forks */
  vector<Philosopher> philosophers; /* The philosophers */
  vector<thread> threads;   /* The threads */

  int numEating{};
  /** Set the table for the philosophers  
	* First sets the forks and then place the philosophers around the forks  
  */
  void setUpTable();

  /**   
   * Let each philospher start the eating process  
	* 
  */
  void eat();
  


public:
  /** Initializes a new instance of the Simulator class */
  Simulator();

  /**
   * Initializes a new instance of the Simulator class
   *
   * @param	nPhilosophers	The philosophers.
   * @param	nEatingRounds	The eating rounds per philosopher.
   * @param	logFile		 	The log file.
   */

  Simulator(const int &nPhilosophers, const int &nEatingRounds,
            const string &logFile);
  /** Finalizes an instance of the Simulator class */
  ~Simulator() = default;
  /** Runs this Simulator */
  void run();
};

#endif // !SIMULATOR_H

/**
// End of Simulator.h
 */

