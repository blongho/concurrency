#include "Philosopher.h"
#include <chrono>
#include <thread>

Philosopher::Philosopher(const string &name, const int &nplates, Fork &lfk,
                         Fork &rfk, std::ofstream &ofstr, int &eaters)
    : philosopherName{name},
      numOfPlatesToEat{nplates}, leftFork{lfk}, rightFork{rfk}, ofs{ofstr}, numEating(eaters) {
  status = PhilosopherState::JOIN_TABLE;
  numEating = 0;
  LogPrinter::logAndPrint(ofs, philosopherName + statusDetails());
}



void Philosopher::operator()() {
  while (!isFinishedEating()) {
	  if (eatCycle == 0) {
		  think();
	  }
	 // acquire mutexes safely
    std::lock(leftFork.getMutex(), rightFork.getMutex());
    eat(); // Adopts the locks and releases them when finsihed
    if (isFinishedEating())
      break;
    think();
  }
}

void Philosopher::eat() {
  // change state to grap
  status = PhilosopherState::GRAP;
  string forks = leftFork.getId() + " and #" + rightFork.getId();
  LogPrinter::logAndPrint(ofs, philosopherName + statusDetails(forks));

  // attempt to get the left lock and if successful,
  //  set status to hungry and log it
  std::lock_guard<std::mutex> leftLock(leftFork.getMutex(), std::adopt_lock);
  status = PhilosopherState::HUNGRY;
  LogPrinter::logAndPrint(ofs,
                          philosopherName + statusDetails(leftFork.getId()));

  std::lock_guard<std::mutex> rightLock(rightFork.getMutex(), std::adopt_lock);
  LogPrinter::logAndPrint(ofs, philosopherName + " grapped fork #" + forks);

  // Successfully grapped both forks? Eat and increase eatCycle,
  // log and print status
  status = PhilosopherState::EAT;
  eatCycle += 1;
  numEating += 1;

  LogPrinter::logAndPrint(ofs, philosopherName + statusDetails());
  
  // Don't hurry, take some random time (1-5 seconds) to enjoy
  const int eatduration = generateRandomInteger(minSecs, maxSecs);
  std::this_thread::sleep_for(std::chrono::seconds(eatduration));

  numEating -= 1;
  // After meal, check status, log and print
  status = (!isFinishedEating()) ? PhilosopherState::DROP_FORKS
                                 : PhilosopherState::FINISHED;
  
  LogPrinter::logAndPrint(ofs, philosopherName + statusDetails(forks));
}

void Philosopher::think() {
  status = PhilosopherState::THINK;
  const int thinkingDuration = generateRandomInteger(minSecs, maxSecs);
  LogPrinter::logAndPrint(ofs, philosopherName + statusDetails());
  std::this_thread::sleep_for(std::chrono::seconds(thinkingDuration));
}

string Philosopher::statusDetails(const string &forkId) const {
	switch (status) {
	case PhilosopherState::JOIN_TABLE:
		return " joined the table and assigned forks (left =>" + leftFork.getId() + ", right =>" + rightFork.getId() + ")";
	case PhilosopherState::THINK:
		return " is THINKING [Philosophers eating: " + std::to_string(numEating) + "]";
	case PhilosopherState::GRAP:
		return " is attempting to GRAP fork#" + forkId + " => [Philosophers eating: " + std::to_string(numEating) + "]";
	case PhilosopherState::HUNGRY:
		return " grapped fork #" + forkId + " and is HUNGRY..." + " [Philosophers eating: " + std::to_string(numEating) + "]";
	case PhilosopherState::EAT:
		return " is EATING round " + std::to_string(eatCycle) + " [Philosophers eating: " + std::to_string(numEating) + "]";
	case PhilosopherState::FINISHED:
		return " is FINISHED [Philosophers eating: " + std::to_string(numEating) + "]";
	case PhilosopherState::DROP_FORKS:
		return " has DROPPED fork #" + forkId + " [Philosophers eating: " + std::to_string(numEating) + "]";
	default:
		return "unknown state";
	}
}


bool Philosopher::isFinishedEating() const {
	return eatCycle == numOfPlatesToEat;
}
