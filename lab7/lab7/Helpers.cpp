#include "Helpers.h"
#include <cstdio>
#include <ctime>

const int generateRandomInteger(const int &min, const int &max) {
	thread_local std::mt19937 mt{ std::random_device{}() };
	std::uniform_int_distribution<int> dist(min, max);
	return dist(mt);
}
