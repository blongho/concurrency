/**
 * project	:	lab7
 * @file   	:	Philosopher.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-28
 * @summary	:	Declares the philosopher class
 * @details	:	A Philopher has as states as detailed in enum class $PhilosopherState
 * 				
 * 				Each Philosopher can have two forks from both sides (left and right)
 * 				and these are the only forks that they can use. 
 * 				
 * 				Read more about the different phases that the Philosopher undergoes
 * 				in the #eat and #think methods of this class
 *
 *
 *				Copyright (c) 2018 Bernard Che Longho.
 **/

#ifndef PHILOSOPHER_H
#define PHILOSOPHER_H

#include "Fork.h"
#include "Helpers.h" // contains default values
#include "LogPrinter.h"
#include <atomic>
#include <iostream>
#include <mutex>
#include <string>
using std::string;  /* The standard string */

/** Values that represent philosopher states */
enum class PhilosopherState {
	JOIN_TABLE, /*When a Philosopher is newly created*/
	THINK,   /* The Philosopher is thinking. Maybe to eat or after eating*/
	GRAP,    /* The Philosopher is attempting to grap a fork*/
	HUNGRY, /*The Philosopher has a fork and is waiting for the other*/
	EAT,     /*  The Philosopher has grapped both forks and is eating*/
	DROP_FORKS,	/*The Philosopher drops both forks*/
	FINISHED /*  The Philosopher has finished eating*/
};


/** A philosopher. */
class Philosopher {
private:
  string philosopherName;   /* Name of the philosopher */
  int numOfPlatesToEat;		/* Number of plates to eat */
  Fork &leftFork;			/* The left fork */
  Fork &rightFork;			/* The right fork */
  std::ofstream &ofs;		/* The ofstream object  */
  PhilosopherState status;  /* The status */
  int& numEating;

  /**
	* The number of times that the Philosopher has eaten. 
	* When the Philosopher enters the EAT state, this number
	* is incremented. When the eatCycle == numOfPlatesToEat
	* Philosopher enters FINISHED state
  */
  int eatCycle{};  /* The eat cycle */

  /**
   * Get the string representation of the Philospher's current status
   *
   * @param	forkId	Identifier for the fork.
   *
   * @return	A string.
   */

  string statusDetails(const string &forkId = "") const;

  /**
   * Query if this Philosopher is finished eating
   *
   * @return	True if eatCycle == numOfPlatesToEat.
   */

  bool isFinishedEating() const;

  /** The philosopher enters the eating phase  
	* This is done as follows
	* i. Attempts to grap both forks
	* ii. After grapping both forks, starts eating immediately
	* iii. Eating occurs during a randomly generated time between 1 and 5 seconds
	* iv. After eating, Philosopher changes state to either
	*		- THINK => Philosopher still has plates to eat  
	*		- FINISH => Philosopher has finished eating all assigned plates  
	* v. The information is logged to file and printed to the user
  */
  void eat();

  /** The Philosopher is thinking  
	* This occurs in a random period of 1 to 5 seconds  
	* This is logged to file and the user is informed of what the   
	* Philosopher is doing. 
  */
  void think();

public:

	/**
	 * Initializes a new instance of the Philosopher class
	 *
	 * @param	name   	The name of the Philosopher.
	 * @param	nplates	The number of plates that the Philosopher has to eat.
	 * @param	lfk	   	The left fork.
	 * @param	rfk	   	The the right fork.
	 * @param	ofstr  	The the logging filestream to log information.
	 */

	Philosopher(const string &name, const int &nplates, Fork &lfk, Fork &rfk, std::ofstream &ofstr, int &eaters);

	/** Finalizes an instance of the Philosopher class */
	~Philosopher() = default;


	 /** Function call operator */
	 void operator()();

};
#endif // !PHILOSOPHER_H
/**
// End of Philosopher.h
 */

