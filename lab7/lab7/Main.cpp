/**
 * @file	:	Main.cpp.
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	:	2019-01-04
 * @brief	:	Runs the simulator
 *
 * @details	:	Use: see the Run section of readme.md file

 */

#include "Simulator.h"
/**
 * Main entry-point for this application
 *
 * @param	args	The number of command-line arguments provided.
 * @param	argv	An array of command-line argument strings.
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 */

int main(int args, char **argv) {

  // No arguments given, run default program
  if (args == 1) {
    // inform user of what is about to happen
    std::cout << "\n\nRunning the simulator with " << defaultNumOfPhilosophers
              << " Philosophers with each eating " << defaultNumOfPlates
              << " times.\nLog file is \"" << defaultLogFile << "\"\n\n"
              << std::endl;
	auto simulator = make_shared<Simulator>();
    simulator->run();
    return 0;
  }

  int nPhilosophers{defaultNumOfPhilosophers};
  int nPlates{defaultNumOfPlates};
  string logfile{defaultLogFile};

  vector<string> arguments(argv, argv + args);

  if (arguments.size() == 7) {
    // attempt to get number of philosophers
    try {
      nPhilosophers = std::stoi(arguments[2]);

      // is this value within range specified by program (4-14) see Helpers.h
      if (nPhilosophers < minNumOfPhilosophers ||
          nPhilosophers > maxNumOfPhilosophers) {
        std::cout << "Number of Philosophers range between ["
                  << minNumOfPhilosophers << " - " << maxNumOfPhilosophers
                  << "]\nUsing the default value [" << defaultNumOfPhilosophers
                  << "]" << std::endl;
        nPhilosophers = defaultNumOfPhilosophers;
      }
    } catch (const std::exception &) { // an error occured
      std::cerr << "Bad value entered for number of philosphers ["
                << arguments[2] << "]\nUsing [" << defaultNumOfPhilosophers
                << "] instead\n\n";
    }

    // attempt to get number of eat cycles
    try {
      nPlates = std::stoi(arguments[4]);

      // is this value within range specified by program (4-10) see Helpers.h
      if (nPlates < minNumOfPlates || nPlates > maxNumOfPlates) {
        std::cout << "Number of plates range between [" << minNumOfPlates
                  << " - " << maxNumOfPlates << "]\nUsing the default value ["
                  << defaultNumOfPlates << "]" << std::endl;

        nPlates = defaultNumOfPlates;
      }
    } catch (const std::exception &) {
      std::cerr << "Bad value entered for number of plates [" << arguments[4]
                << "]\nUsing [" << defaultNumOfPlates << "] instead\n\n";
    }

    // atempt to get a file name
    try {
      logfile = arguments[6];
      if (logfile.empty()) {
        std::cout << "Log file cannot be empty. Using the default file ["
                  << defaultLogFile << "]\n";
      }
    } catch (const std::exception &) {
      std::cerr << "Bad value entered for filename [" << arguments[6] << "]\n"
                << "Using [" << defaultLogFile << "] instead\n\n";
    }
  }      // END IF
  else { // user failed to enter the commands as needed
    std::cout << "\n\nUsage ./lab7 -n <numOfPhilosophers> -d "
                 "<numOfPlatesPerPhilosopher> -f <logFileName>\n"
              << "Using default values" << std::endl;
  }

  // inform user of what is about to happen
  std::cout << "\n\nRunning the simulator with " << nPhilosophers
            << " Philosophers with each eating " << nPlates
            << " times. Log file is \"" << logfile << "\"\n\n"
            << std::endl;

  // Run the program
  auto simulator = make_shared<Simulator>(nPhilosophers, nPlates, logfile);

  simulator->run();

  return 0;
}
