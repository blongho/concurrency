/**
 * project	:	lab7
 * @file   	:	Fork.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-29
 * @summary	:	Declares the fork class
 * 				This class holds the information about a fork (the
 *				fork number and a mutex that locks the fork)
 *
 *				Copyright (c) 2018 Bernard Che Longho. 
 **/

#ifndef FORK_H
#define FORK_H
#include <iostream>
#include <mutex>
#include <string>
using std::mutex;  /* The standard mutex */
using std::string; /* The standard string */

/** A fork. */
class Fork {
private:
  mutex mtx; /* The mtx */
  string id; /* The identifier */
public:
  /**
   * Initializes a new instance of the Fork class
   *
   * @param	forkId	Identifier for the fork.
   */

  Fork(const string &forkId);
  /** Finalizes an instance of the Fork class */
  ~Fork() = default;

  /**
   * Gets the identifier
   *
   * @return	The identifier.
   */

  string getId() const;

  /**
   * Gets the mutex
   *
   * @return	The mutex.
   */

  mutex &getMutex();
};
#endif // !FORK_H

/**
// End of Fork.h
 */
