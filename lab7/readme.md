# The Dining Philosopher Problem in C++
## Program description
### Summary
This program attempts to simulate the [Dining philosophers problem](https://en.wikipedia.org/wiki/Dining_philosophers_problem) using multithreading in C++.

The program is built such that it can take any number of philosophers between
4 and 14 and each Philosopher can eat between 4 and 10 times.

When a Philosopher is created, they join the table, and then think for a random
amount of seconds between 1 to 5. When a Philosopher wants to eat, s(he) attempts
to grap the forks. If both forks are available, the Philosopher eats for some
random period between 1 and 5 seconds. After eating, the Philosopher drops both
forks and thinks again before eating.

Every state of the Philosopher is logged into a file.

When the program runs, each Philosopher runs in a separate thread.

The Program ends when every philosopher has eating all of their plates.

### Class summaries
#### Philosopher
A Philosopher is represented by the ```class Philosopher```. Each Philosopher
has a name, knows the forks it is going to use, the number of plates s(he) has
to eat and knows a file where it logs all its different actions during the
program flow.
- PhilosopherState, an enum class

The Philosopher can be in 7 different states which are detailed below. <br>
```cpp
/** Values that represent philosopher states */
enum class PhilosopherState {
	JOIN_TABLE, /*When a Philosopher is newly created*/
	THINK,   /* The Philosopher is thinking. Maybe to eat or after eating*/
	GRAP,    /* The Philosopher is attempting to grap a fork*/
	HUNGRY, /*The Philosopher has a fork and is waiting for the other*/
	EAT,     /*  The Philosopher has grapped both forks and is eating*/
	DROP_FORKS,	/*The Philosopher drops both forks*/
	FINISHED /*  The Philosopher has finished eating*/
};
```
#### Fork
Each Philosopher is assigned a fork (left and right) which are the only forks
that they can use. A fork is represented by the ```class Fork```. This class
has a `mutex` which acts as the waiter for each Philosopher. Each fork also has
an id so that it can be identified and assigned properly.

#### LogPrinter
Information logging and printing is accomplished by a the ```LogPrinter class```.
This class has just one variable, a `mutex` which ensures mutual exclusion for
saving to file and printing to screen. It has a public member function
`LogPrinter::logAndPrint()` that takes the `ofstream` object amd the information
to print and log.


#### Simulator
This class is aimed at decluttering the `Main.cpp` file. All the setup of the
Philosopher and running of the Philosopher class is encapsulated in this class.

The Simulator requires the number of philosophers (`4 - 14` or default `6`), the
number of plates for each philosopher (`4 - 10` or default `5`) and the name of
the log file (default `philo.log`)

Apart from the constructor, this class has one public method
 ```cpp
Simulator::run()
```

##### Helpers[.h|.cpp]
This is a helper file for holding all global variables and some global functions
that are not a direct responsiblity of any class

## How to run
The project is comes with a Makefile and as a packaged Visual studio project
1. Using make <br>
    i. `make` <br>
	ii. If windows, the executable will be `lab7.exe`, in unix it will be `lab7` <br>
	iii. ``./lab7 -n <numOfPhilosophers> -d <numOfPlates> -f <logFileName>`` <br>

2. In Microsoft Visual Studio  
	i. Click the lab7.sln and the project will be created in your local copy of
    Microsoft visual studio. This project uses MVS2017 Enterprise with toolset
    as in [project tool set]("./lab7/images/project_tool_set.PNG") <br>
    To set command line arguments in Visual studio, do the following
    click `projectName >> properties >> Configuration Properties >> Debugging >>
    Local Windows Debugger >> Command Arguments`. <br>
    Like [this]("./lab7/images/setting_command_line_args_in_vs.PNG")

Variables needed are <br>
|variable			|range		  |	default value|
|:------------------|:-----------:|:------------:|
|numOfPhilosophers	|[4 - 14]	  |	6			 |
|numOfPlates		|[4 - 10]	  |	5			 |
|logFileName		|[any string] |	philo.log	 |

If any of these is missing or out or range, the default values are used.<br>
If none is given, the program uses the default values.<br>
In either case, the use if informed of the parameters being used.<br>
