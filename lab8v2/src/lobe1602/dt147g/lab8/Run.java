/**
 * lab8 lobe1602.dt147g.lab8 Run.java
 * 
 * longb Jan 12, 2019
 * 
 */
package lobe1602.dt147g.lab8;

import javax.swing.SwingUtilities;

import lobe1602.dt147g.lab8.gui.PrimeSeekerDisplay;

/**
 * @author longb
 *
 */
public class Run {

	/**
	 * 
	 */
	public Run() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length == 3) {
			for (int i = 0; i < args.length; i++) {
				System.out.println("arg#" + i + " : " + args[i]);
			}
			try {
				long max = Integer.valueOf(args[1]);
				long chunksize = Integer.valueOf(args[2]);
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						PrimeSeekerDisplay display = new PrimeSeekerDisplay(max,
								chunksize);
						display.setVisible(true);
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("\nUsage: lab8 maxValue chunkSize\n");
		}
	}

}
