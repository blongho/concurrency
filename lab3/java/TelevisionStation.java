/**
* @file 	: TelevisionStation.java
* @author   : Bernard Che Longho (lobe1602)
* @since    : 2018-11-25
**/
package concurrency.ch03_sharing_objects.exercise_3_2;

import java.text.*;
import java.util.*;

import net.jcip.annotations.GuardedBy;

public class TelevisionStation extends MediaCompany {
    private final static DateFormat dayFormat =
            new SimpleDateFormat("yyyy-MM-dd");

    /**
     * This is not thread-safe. It is good practice to make this file
     * private so that it can not be changed from other classes like
     * TelevisionStation station = new TelevisionStation();
     * Map<String, Collection<String>> headlines = station.headlines;
     *
     * This field should therefore be private.
     *
     * Any updates to this container should by synchronize therefore
     * we use @GuardedBy("this"), telling this class to protect it
     * intrinsically
     */
    /*public Map<String, Collection<String>> headlines =
            new HashMap<>(); */

    @GuardedBy("this")
    private Map<String, Collection<String>> headlines =
            new HashMap<>();

    public TelevisionStation() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                if (headlines.size() > 100) {
                    System.out.println("Size of headlines too large: " +
                            headlines.size());
                }
            }
        }, 10_000, 10_000);
    }

    /*
     * In this method, i think the printing should be synchronized. Since we
     * are printing the values from headlines, we must make sure that the values
     * that we get are in sync.
     */
    public synchronized void showTeleprompter() {
        printHeadlines(headlines.values());
    }

    public void addHeadline(Date date, String line) {
		String day = dayFormat.format(date);
		/**
		 * This block has to be synchronized otherwise, we might be accessing the wrong
		 * values in headlines. Getting values from $headlines and updating same should
		 * be guarded with the use of synchronization
		 */

		Collection<String> lines = new ArrayList<String>(); // make line invariant

		synchronized (this) {
			lines = headlines.get(day); // synchronously set values of lines
		}

		// if we now have some items in lines, we can add them to headlines
		if (!lines.isEmpty()) {
			synchronized (this) {
				headlines.put(day, lines); // synchronized update of headlines
			}
		}

		lines.add(line);
	}
}
