/**
 * project	:	lab5
 * @file   	:	Consumer.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-05
 * @brief	:	Declares the consumer class
 * @details :   The Consumer get the items from the queue and put in an internal
 * container for later processing
 *
 *				Copyright (c) 2018 Bernard Che Longho. All rights reserved.
 **/

#ifndef CONSUMER_H
#define CONSUMER_H

#include "Utils.h"
#include <memory>

/**
 * @brief An alias for the shared queue
 */
typedef std::shared_ptr<std::deque<size_t>> SharedConsumption;

class Consumer
{
private:
    Buffer &queue;	/** The shared queue */
    size_t items{};   /** number of items expected to consume */
    SharedConsumption consumed = std::make_shared<std::deque<size_t>>();
public:
    Consumer(Buffer &sharedQueue, const size_t &pItems);
    ~Consumer() {}
    SharedConsumption operator()();
};

#endif // !CONSUMER_H

/**
// End of Consumer.h
 */

