#include "Utils.h"
#include "ThreadSafePrinter.h"
using std::to_string;

typedef  ThreadSafePrinter Printer;

ValuePairs extractUserInput(int args, char** argv)
{
    ValuePairs pairs{};
    if(args != 4){
        return pairs;
    }else{
        size_t elements{0};
        size_t producers{0};
        size_t consumers{0};

        // first try to capture the value as int so that we could get negative
        // values. If the value is not negative, cast it to size_t otherwise,
        // the values of elemements, producers and consumers stay as zero
        try {
            int tmp = std::stoi(argv[1]); // capture negative values
            if(tmp > 0 && tmp < INT32_MAX){
                elements = static_cast<size_t>(tmp);
            }

        } catch (std::invalid_argument &) {
            Printer::println({"Bad value entered for elements [", argv[1], "]"});
        }
        try {
            int tmp = std::stoi(argv[2]); // capture negative values
            if(tmp > 0 && tmp < INT32_MAX){
                producers = static_cast<size_t>(tmp);
            }
        } catch (std::invalid_argument &) {
            Printer::println({"Bad value entered for producers [", argv[2], "]"});
        }
        try {
            int tmp = std::stoi(argv[3]); // capture negative values
            if(tmp > 0 && tmp < INT32_MAX){
                consumers = static_cast<size_t>(tmp);
            }
          } catch (std::invalid_argument &) {
            Printer::println({"Bad value entered for consumers [", argv[2], "]"});
        }
        pairs[keyElement] = elements;
        pairs[keyProducers] = producers;
        pairs[keyConsumers] = consumers;
    }
    return pairs;
}

void informUser(const size_t &elements, const size_t &producers, const size_t &consumers)
{
    const std::string msg = "Handling " + to_string(elements)
            + " values with " + to_string(producers) + " producers and "
            + to_string(consumers) + " consumers";

    Printer::println();
    Printer::println(msg);
    Printer::println();
}
