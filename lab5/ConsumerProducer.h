/**
 * project	:	lab5
 * @file   	:	ConsumerProducer.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-05
 * @brief	:	Declares the ConsumerProducer class
 *
 *				Copyright (c) 2018 Bernard Che Longho. All rights reserved.
 **/
#ifndef CONSUMERPRODUCER_H
#define CONSUMERPRODUCER_H
#include <thread>
#include <future>
#include "Consumer.h"
#include "Producer.h"
#include "ThreadSafePrinter.h"

using std::to_string;
typedef ThreadSafePrinter Printer;

class ConsumerProducer
{
private:
    Buffer &sharedQueue; /** The shared resource */
    size_t nElements{defaultElements}; /** The number of elements to be produced */
    size_t nProducers{defaultProducers}; /** The number of producer threads to create */
    size_t nConsumers{defaultConsumers}; /** The number of consumers */
    std::vector<std::thread> producerThreads;
    std::vector<std::future<SharedConsumption>> consumerThreads;
    std::vector<SharedConsumption> consumptionResults();
    void loadProducers();
    void loadConsumers();
    size_t getExpectedSum() const;

public:
    ConsumerProducer(Buffer &buffer,
                     const size_t &elems,
                     const size_t & pr,
                     const size_t &con);
    /**
     * @brief The start and end of the the program flow
     */
    void run();
};

#endif // CONSUMERPRODUCER_H
