TEMPLATE = app
CONFIG += console c++11 -Wall -O3 -march=native -ffast-math -formit-frame-pointer
CONFIG += thread -flto -fwhole-program
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    Consumer.cpp \
    Main.cpp \
    Producer.cpp \
    ConsumerProducer.cpp \
    Utils.cpp \
    ThreadSafePrinter.cpp

HEADERS += \
    Consumer.h \
    Producer.h \
    ConsumerProducer.h \
    BoundedBlockingBuffer.hpp \
    Utils.h \
    ThreadSafePrinter.h

DISTFILES += \
    Makefile
