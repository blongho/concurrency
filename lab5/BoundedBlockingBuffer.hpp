/**
 * project	:	lab5
 * @file   	:	BoundedBlockingQueue.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-07
 * @brief	:	Declares the bounded blocking queue class
 *
 *				Copyright (c) 2018 Bernard Che Longho. All rights reserved.
 **/

#ifndef BOUNDED_BLOCKING_BUFFER_HPP
#define BOUNDED_BLOCKING_BUFFER_HPP
#include <deque>    // std::deque<T>
#include <condition_variable> // std::condition_variable
#include <iostream>

const  size_t bufferSize{10};

template <typename T>
class BoundedBlockingBuffer
{
private:
    std::deque<T> buffer;	/** The container */
    std::mutex mutex;   /** The mutex lock */
    size_t maxSize{bufferSize};
    std::condition_variable cv;
public:
    BoundedBlockingBuffer() {}

    /**
     * @brief BoundedBlockingBuffer constructor to determine the max size of
     * the buffer
     * @param maxSz
     */
    BoundedBlockingBuffer(const size_t &maxSz)
        :maxSize(maxSz){}

    /**
     * @brief A copy constructor
     * @param other
     */
    BoundedBlockingBuffer(BoundedBlockingBuffer &other)
    {
        std::lock_guard<std::mutex> lock(other.mutex);
        buffer = other.buffer;
    }

    /**
     * @brief Get an item at the front of the queue
     * @return  return the first item that was inserted
     */
    T take()
    {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [this](){return !buffer.empty();});
        const T item = buffer.front();
        buffer.pop_front();
        lock.unlock();
        cv.notify_one();
        return item;
    }

    /**
     * @brief put   Insert an item at the back of the queue
     *              Lock if max size is reached
     * @param value the value to insert in the container
     */
    void put(const T &value)
    {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [this](){return maxSize > buffer.size(); });
        buffer.push_back(value);
        lock.unlock();
        cv.notify_one();
    }

};
#endif // BOUNDEDBLOCKINGBUFFER_HPP
