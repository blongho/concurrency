﻿#ifndef THREAD_SAFE_PRINTER_H
#define THREAD_SAFE_PRINTER_H
#include <mutex>
#include <string>
#include <iostream>
#include <vector>

class ThreadSafePrinter
{
private:
    ThreadSafePrinter();
public:
    static void println(const std::string &something = "");
    static void print(const std::string &something = " ");
    static void println(const std::vector<std::string> &items);

    static void println(const size_t &something);
    static void print(const size_t &something);
    static void println(const std::vector<size_t> &items);

    static void println(const signed &something);
    static void print(const signed &something);
    static void println(const std::vector<signed> &items);

};


#endif // THREADSAFEPRINTER_H
