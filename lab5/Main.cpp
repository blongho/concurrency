﻿/**
  * @file   Main.cpp
  * @author Bernard Che Longho (lobe1602@student.miun.se)
  * @since  2018-12-17
  * @details
  * This file contains the entry and exit point of lab5: An implementation of
  * the Producer-Consumer pattern using a BoundedBlockingQueue
  *
  * The program starts by finding command line arguments entered by the
  * user using the utility function extractUserInput()
  * If the user never entered anything, then the default values for number of
  * elements, number of Producers and number of consumers are used.
  * These default values are defined in Utils.h file
  *
  * If any of the command line arguments are not given or a negative number is
  * entered, the program terminates and informs the user of the reason
  *
  * To run this on the command line, do
  * make && lab5
  * or
  * make && lab5 elements producers consumers
  * eg make && lab5 100 6 6
  *
  * The program comes with a lab5.pro file for recreating the same program in Qt
  * creator
  * */
#include "ConsumerProducer.h"
//#include <memstat.hpp>
using std::string;
using std::cout;
using std::endl;
int main(int args, char**argv)
{

    ValuePairs pairs = extractUserInput(args, argv);
    Buffer buffer;
    // no inputs were sent, run default values
    if(pairs.empty()){
        auto program = std::make_shared<ConsumerProducer>(
                    ConsumerProducer(std::ref(buffer),
                                     defaultElements,
                                     defaultProducers,
                                     defaultConsumers));

        informUser(defaultElements, defaultProducers, defaultConsumers);
        program->run();
    }
    else{ // user entered some values, get them
        size_t elements = pairs[keyElement];
        size_t producers = pairs[keyProducers];
        size_t consumers = pairs[keyConsumers];

        /*
        * If any of these values is zero, end the exit the program
        * A zero here means that either
        * 1) an invalid argument exception was thrown or
        * 2) a negative value was entered or
        * 3) a number greater than INT32_MAX
        *
        * */
        if(elements == 0  || producers == 0 || consumers == 0){
            Printer::println("All values must be positive integers");
            Printer::println("Exiting program....");
            return 1;
        }

        auto program  = std::make_shared<ConsumerProducer>(
                    ConsumerProducer(std::ref(buffer),
                                     elements,
                                     producers,
                                     consumers));

        informUser(elements, producers, consumers);

        program->run();
    }
    return 0;
}
