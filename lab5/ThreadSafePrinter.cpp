#include "ThreadSafePrinter.h"
static std::mutex mutex;

void ThreadSafePrinter::println(const std::string &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something << std::endl;

}

void ThreadSafePrinter::print(const std::string &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something ;
}

void ThreadSafePrinter::println(const std::vector<std::string> &items)
{
   // std::lock_guard<std::mutex> lock(mutex);
    for(auto &item: items){
        std::cout << item << " ";
    }
    std::cout << std::endl;
}

void ThreadSafePrinter::println(const size_t &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something << std::endl;
}

void ThreadSafePrinter::print(const size_t &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something ;
}

void ThreadSafePrinter::println(const std::vector<size_t> &items)
{
    //std::lock_guard<std::mutex> lock(mutex);
    for(auto &item: items){
        std::cout << item << " ";
    }
    std::cout << std::endl;
}

void ThreadSafePrinter::println(const signed &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something << std::endl;
}

void ThreadSafePrinter::print(const signed &something)
{
    //std::lock_guard<std::mutex> lock(mutex);
    std::cout << something ;
}

void ThreadSafePrinter::println(const std::vector<signed> &items)
{
    //std::lock_guard<std::mutex> lock(mutex);
    for(auto &item: items){
        std::cout << item << " ";
    }
    std::cout << std::endl;
}
