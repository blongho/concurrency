﻿#include "ConsumerProducer.h"
#include <numeric> // std::accumulate
using std::ref;
using std::cout;
using std::endl;


ConsumerProducer::ConsumerProducer(Buffer &buffer,
                                   const size_t &elems,
                                   const size_t & pr,
                                   const size_t &con)
    :sharedQueue(buffer), nElements(elems), nProducers(pr), nConsumers(con){
}


std::vector<SharedConsumption> ConsumerProducer::consumptionResults()
{
    std::vector<SharedConsumption> results;
    for(auto &fu: consumerThreads){
        Printer::print("Waiting... ");
        fu.wait();
        results.push_back(fu.get());
        Printer::println("got a result");
    }
    return results;
}

void ConsumerProducer::loadProducers()
{
    const size_t remainder = nElements % nProducers;
    const size_t normal = nElements/nProducers;

    for (size_t index = 0; index < nProducers; index++) {
        const size_t min = index * normal;
        size_t max = min + normal;
        if(index == nProducers -1){
            max += remainder;
        }
        producerThreads.emplace_back(std::thread(Producer(ref(sharedQueue), min, max)));

    Printer::println("Starting a producer for " + to_string(max-min)
                         + " values, interval [" + to_string(min) + "..."
                         + to_string(max) + ")");
    }

    Printer::println("\nWorking behind the scene, please wait...\n");

}

void ConsumerProducer::loadConsumers()
{
    const size_t remainder = nElements % nConsumers;
    const size_t normal = nElements/nConsumers;
    const size_t oneBig = normal + remainder;

    for(size_t index = 0; index < nConsumers; index++){
        size_t items = (index == nConsumers - 1)? oneBig: normal;
        consumerThreads.emplace_back(std::async(std::launch::async,
                                                Consumer(ref(sharedQueue), items)));
    }
}

size_t ConsumerProducer::getExpectedSum() const
{
    size_t sum{};
    for (size_t var = 0; var < nElements; ++var) {
        sum += var;
    }
    return sum;
}


void ConsumerProducer::run()
{
    loadProducers();
    loadConsumers();

    std::vector<SharedConsumption> results = consumptionResults();

    size_t itemsReceived{};
    size_t sumOfItems{};

    for(auto &result: results){
        itemsReceived += result->size();
        sumOfItems += std::accumulate(result->begin(), result->end(), size_t(0));
    }

    // print statistics
    Printer::println();
    Printer::println("Got " + to_string(itemsReceived) + " values");
    Printer::println();

    Printer::println("Expected sum: " + to_string(getExpectedSum()));
    Printer::println("Calculated sum: " + to_string(sumOfItems));
    Printer::println();

    // join the producers
    for(auto &pthread: producerThreads){
        if(pthread.joinable()){
            pthread.join();
            Printer::println("Producer thread joined");
        }else{
            Printer::println("Could not join one producer");
        }
    }

    Printer::println();
    Printer::println("======= END OF PROGRAM ========");
}
