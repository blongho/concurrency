#ifndef UTILS_H
#define UTILS_H
#include "BoundedBlockingBuffer.hpp"
#include <map>

typedef  BoundedBlockingBuffer<size_t> Buffer;
typedef std::map<std::string, size_t> ValuePairs;

ValuePairs extractUserInput(int args, char** argv);

void informUser(const size_t &elements, const size_t &producers, const size_t &consumers);

const std::string keyElement{"elements"};
const std::string keyProducers{"producers"};
const std::string keyConsumers{"consumers"};

const size_t defaultElements = 10000000;
const size_t defaultProducers = 6;
const size_t defaultConsumers = 6;

#endif // CONSTANTS_H
