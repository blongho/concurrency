/**
 * project	:	lab5
 * @file   	:	Producer.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2018-12-05
 * @brief	:	Declares the producer class
 *
 *				Copyright (c) 2018 Bernard Che Longho. All rights reserved.
 **/

#ifndef PRODUCER_H
#define PRODUCER_H
#include "Utils.h"

/** A producer. */
class Producer{
private:
    Buffer &queue;	/** The queue */
    size_t min{};
    size_t max{};
public:
    /**
     * Initializes a new instance of the Producer class
     *
     * @param	pItems	The items.
     */

   Producer(Buffer &sharedQueue, const size_t &lower, const size_t &upper);

    ~Producer() {}
    /** Runs this Producer */

    size_t getMin() const;
    void setMin(const size_t &value);
    size_t getMax() const;
    void setMax(const size_t &value);
    void operator()();
};


#endif // !PRODUCER_H

/**
// End of Producer.h
 */

