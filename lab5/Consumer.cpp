#include "Consumer.h"
#include <iostream>

Consumer::Consumer(Buffer& sharedQueue, const size_t &pItems)
    :queue(sharedQueue), items(pItems){}

SharedConsumption Consumer::operator()()
{
    for (size_t i = 0; i < items; i++)
    {
        const size_t item = queue.take();
        consumed->push_back(item);
    }
    return consumed;
}
