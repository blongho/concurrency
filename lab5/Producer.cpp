﻿#include "Producer.h"
#include <iostream>

Producer::Producer(Buffer &sharedQueue, const size_t &lower, const size_t &upper)
    : queue(sharedQueue), min(lower), max(upper){}


size_t Producer::getMin() const
{
    return min;
}

void Producer::setMin(const size_t &value)
{
    min = value;
}

size_t Producer::getMax() const
{
    return max;
}

void Producer::setMax(const size_t &value)
{
    max = value;
}

void Producer::operator()()
{
    for(size_t i = min; i < max; i++)
    {
        queue.put(i);
    }
}
