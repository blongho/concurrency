/**
 * @project lab6
 * @package lobe1602.lab6
 * @file ExecutionInterface.java
 * 
 * @author Bernard Che Longho (lobe1602@student.miun.se)
 * @since Dec 14, 2018
 * 
 * @brief An interface for all Execution classes.
 * 
 */
package lobe1602.lab6.strategies;

/**
 * @author longb
 *
 */
public interface ExecutionInterface {
	public abstract void execute();
}
