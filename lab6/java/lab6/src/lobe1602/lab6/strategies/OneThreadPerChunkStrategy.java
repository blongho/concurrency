/**
 * @project lab6
 * @package lobe1602.lab6.strategies
 * @file OneThreadPerChunkStrategy.java
 * 
 * @author Bernard Che Longho (lobe1602@student.miun.se)
 * @since longb Dec 14, 2018
 * @brief This class executes chunks of work, one thread per chunk It
 *        uses the ExecutorService
 */
package lobe1602.lab6.strategies;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import lobe1602.lab6.contants.Constants;
import lobe1602.lab6.worker.ChunkOfWork;

/**
 * @author longb
 *
 */
public class OneThreadPerChunkStrategy implements ExecutionInterface {

	/**
	 * 
	 */
	public OneThreadPerChunkStrategy() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see lobe1602.lab6.ExecutionStrategy#execute()
	 */
	@Override
	public final void execute() {
		System.out.println(getClass().getSimpleName());
		System.out.println(
				"#threads == #ChunkOfWork == " + Constants.N_CHUNKS + " ...");

		List<CompletionService<Double>> tasks = new ArrayList<>();

		double sum = 0.0;

		final Instant start = Instant.now();

		for (int i = 0; i < Constants.N_CHUNKS; i++) {
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(
					Executors.newSingleThreadExecutor());
			completionService.submit(new ChunkOfWork());
			tasks.add(completionService);
		}
		try {
			for (CompletionService<Double> task : tasks) {
				sum += task.take().get();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final Instant stop = Instant.now();

		final double average = sum / Constants.N_CHUNKS;
		final long duration = Duration.between(start, stop).toMillis();

		System.out.println("Mean	: " + average);
		System.out.println("Duration: " + duration + " ms");

	}
}
