/**
 * @project lab6
 * @package lobe1602.lab6
 * @file ThreadPoolStrategy.java
 * 
 * @author Bernard Che Longho (lobe1602@student.miun.se)
 * @since Dec 14, 2018
 * @brief This class executes a chunk of work using a fixed number of
 *        threads that is specified in its constructor
 * 
 */
package lobe1602.lab6.strategies;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import lobe1602.lab6.contants.Constants;
import lobe1602.lab6.worker.ChunkOfWork;

/**
 * @author longb
 *
 */
public class ThreadPoolStrategy implements ExecutionInterface {

	private final int numOfThreads;

	/**
	 * 
	 */
	public ThreadPoolStrategy() {
		numOfThreads = 0;
	}

	/**
	 * 
	 */
	public ThreadPoolStrategy(final int hundred) {
		numOfThreads = hundred;
	}

	@Override
	public final void execute() {
		System.out
				.print("ThreadPoolStrategy using " + numOfThreads + " threads");
		if (numOfThreads == Constants.CPU_CORES) {
			System.out.print(" == #available processor cores\n");
		}
		System.out.println("\n" + Constants.N_CHUNKS + " ChunkOfWork");

		double sum = 0.0;
		final Instant start = Instant.now(); // start the timer
		final CompletionService<Double> completionService = new ExecutorCompletionService<Double>(
				Executors.newFixedThreadPool(numOfThreads));
		// Load ThreadPool
		for (long i = 0; i < Constants.N_CHUNKS; i++) {
			final ChunkOfWork worker = new ChunkOfWork();
		// futures.add(executorService.submit(worker));
			completionService.submit(worker);
		}

		// start retrieval of results

		try {
			for (int i = 0; i < Constants.N_CHUNKS; i++) {
				sum += completionService.take().get();
			}
		} catch (final InterruptedException e) {
			e.printStackTrace();
		} catch (final ExecutionException e) {
			e.printStackTrace();
		}

		final Instant stop = Instant.now();
		final double average = sum / Constants.N_CHUNKS;
		final long duration = Duration.between(start, stop).toMillis();

		System.out.println("Mean	: " + average);
		System.out.println("Duration: " + duration + " ms");
	}

}
