/**
 * @project lab6
 * @package lobe1602.lab6
 * @file SequentialStrategy.java
 * 
 * @author Bernard Che Longho (lobe1602@student.miun.se)
 * @since longb Dec 14, 2018
 * @brief This class executes chunks of work, one at a time in the main
 *        thread.
 * 
 */
package lobe1602.lab6.strategies;

import java.time.Duration;
import java.time.Instant;

import lobe1602.lab6.contants.Constants;
import lobe1602.lab6.worker.ChunkOfWork;

/**
 * @author longb
 *
 */
public class SequentialStrategy implements ExecutionInterface {

	/**
	 * 
	 */
	public SequentialStrategy() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see lobe1602.lab6.ExecutionStrategy#execute()
	 */
	@Override
	public final void execute() {
		System.out.println(getClass().getSimpleName());
		System.out.println("1 thread (default thread) " + Constants.N_CHUNKS
				+ " ChukOfWork...");

		double sum = 0.0;

		final Instant start = Instant.now();

		for (int i = 0; i < Constants.N_CHUNKS; i++) {
			ChunkOfWork work = new ChunkOfWork();
			try {
				sum += work.call();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		final Instant stop = Instant.now();

		final double average = sum / Constants.N_CHUNKS;
		final long duration = Duration.between(start, stop).toMillis();

		System.out.println("Mean	: " + average);
		System.out.println("Duration: " + duration + " ms");
	}
}
