/**
 * lab6 lobe1602.lab6 ChunkOfWork.java
 * 
 * longb Dec 14, 2018
 * 
 */
package lobe1602.lab6;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author longb
 *
 */
public class ChunkOfWork implements Callable<Double> {
	public static int CHUNK_SIZE = 1000000;

	private final int chunckSize;
	private final String fileName = "tmp";

	public ChunkOfWork() {
		chunckSize = CHUNK_SIZE;
	}

	/**
	 * @param chunckSize
	 */
	public ChunkOfWork(int chunckSize) {
		super();
		this.chunckSize = chunckSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Double call() throws Exception {
		Random random = new Random();
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		List<Double> values = new ArrayList<Double>();

		double sum = 0.0;
		double value;

		for (int i = 0; i < Constants.CHUNK_SIZE; i++) {
			value = random.next();
			sum += value;
			if ((i % Constants.TWO_HUNDRED) == 0) {
				writer.write(String.valueOf(value));
			}
			values.add(value);
			if ((i % Constants.FILE_INTERVAL) == 0) {
				// calculate partial sum
				final double partialSum = values.stream()
						.mapToDouble(Double::doubleValue).sum();
				writer.write(String.valueOf(partialSum));
				values.clear(); // empty the container
			}
		}

		// do house keeping
		try {
			writer.close();
		} catch (Exception e) {
			System.err.println("Error closing file [" + e.getMessage() + ']');
		}

		try {
			new File(fileName).delete();
		} catch (Exception e) {
			System.err.println("Failed to delete file " + fileName);
		}
		return sum / Constants.CHUNK_SIZE;
	}
}
