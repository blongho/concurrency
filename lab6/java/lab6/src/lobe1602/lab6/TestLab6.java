/**
 * @project lab6
 * @package lobe1602.lab6
 * @file 	TestLab6.java
 * 
 * @author	Bernard Che Longho (lobe1602@student.miun.se)
 * @since 	Dec 14, 2018
 * @description
 * 
The program measures the times it takes to produce 500 miljon random double values,
 calculate partial sums and write to a file and write each 10 value to the same file.
 
 The work is partioned in 500 chunks of the same size and each chunk is represented
 by an instance of ChunkOfWork.
 The procedure above is carried 8 times with different execution strategies. 
 
 1. One thread per chunk: 500 threads are started, each one handling one million values
 2. Thread pool: a pool of 100 threads is used to handle 500 chunks of work
 3. Thread pool: a pool of 50 threads is used to handle 250 chunks of work
 4. Thread pool: a pool with number of threads equal to the available number of CPU cores +1
    is used to handle 500 chunks of work.
 5. Thread pool: a pool with number of threads equal to the available number of CPU cores
    is used to handle 500 chunks of work.
 6. Thread pool: a pool with number of threads equal to the available number of CPU cores -1
    is used to handle 500 chunks of work.
 7. Thread pool: a pool with number of threads equal to half of the available number of CPU cores
    on the actual hardware is used to handle 500 chunks of work.
 8  Single thread: sequential execution of all 500 chunks in the main thread

 Each execution is timed.

 * 
 */
package lobe1602.lab6;

import lobe1602.lab6.contants.Constants;
import lobe1602.lab6.strategies.*;

public class TestLab6 {
	static final void lineSeparator() {
		System.out.println("------------------------------------------");
	}

	/**
	 * 
	 */
	public TestLab6() {
		// does nothing as this is never called
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("TEST suite begins for " + Constants.TOT_VALUES + " values");
		System.out.println("ChunkOfWork == " + Constants.CHUNK_SIZE);

		OneThreadPerChunkStrategy serial = new OneThreadPerChunkStrategy();
		serial.execute();
		lineSeparator();
		
		
		ThreadPoolStrategy pool;
		pool = new ThreadPoolStrategy(Constants.HUNDRED);
		pool.execute();
		pool = null;
		lineSeparator();

		pool = new ThreadPoolStrategy(Constants.FIFTY);
		pool.execute();
		pool = null;
		lineSeparator();

		 // #threads equal to hardware cores +1
		final int cores = Constants.CPU_CORES;
		pool = new ThreadPoolStrategy(cores + 1);
		pool.execute();
		pool = null;
		lineSeparator();

		 // #threads equal to hardware cores
		pool = new ThreadPoolStrategy(cores);
		pool.execute();
		pool = null;
		lineSeparator();

		// #threads equal to hardware cores -1
		pool = new ThreadPoolStrategy(cores - 1);
		pool.execute();
		pool = null;
		lineSeparator();

		// #threads equal to hardware cores -1
		pool = new ThreadPoolStrategy(cores / 2);
		pool.execute();
		pool = null;
		lineSeparator();

		
		SequentialStrategy sequence = new SequentialStrategy();
		sequence.execute();
		lineSeparator();
		
		System.out.println("TEST suite ended");

	}

}
