/**
 * lab6 lobe1602.lab6 Constants.java
 * 
 * longb Dec 14, 2018
 * 
 */
package lobe1602.lab6.contants;

/**
 * @author longb
 *
 */
public class Constants {

	public final static int TWO = 2;
	public final static int FIVE = 5;
	public final static int TEN = FIVE * TWO;
	public final static int FIFTY = TEN * FIVE;
	public final static int HUNDRED = FIFTY * TWO;
	public final static int TWO_HUNDRED = HUNDRED * TWO;
	public final static int THOUSAND = TEN * HUNDRED;
	public final static int MILLION = THOUSAND * THOUSAND;
	public final static int TOT_VALUES = FIVE * HUNDRED * MILLION;
	public final static int CHUNK_SIZE = MILLION;
	public final static int N_CHUNKS = TOT_VALUES / CHUNK_SIZE;
	public final static int FILE_INTERVAL = THOUSAND;
	public final static int CPU_CORES = Runtime.getRuntime()
			.availableProcessors();

	/**
	 * 
	 */
	private Constants() {
		// TODO Auto-generated constructor stub
	}

}
