/**
 * lab6 lobe1602.lab6 Constants.java
 * 
 * longb Dec 14, 2018
 * 
 */
package lobe1602.lab6;

/**
 * @author longb
 *
 */
public class Constants {

	public final static long TWO = 2;
	public final static long FIVE = 5;
	public final static long TEN = FIVE * TWO;
	public final static long FIFTY = TEN * FIVE;
	public final static long HUNDRED = FIFTY * TWO;
	public final static long TWO_HUNDRED = HUNDRED * TWO;
	public final static long THOUSAND = HUNDRED * HUNDRED;
	public final static long MILLION = THOUSAND * THOUSAND;
	public final static long TOT_VALUES = FIVE * HUNDRED * MILLION;
	public final static long CHUNK_SIZE = MILLION;
	public final static long N_CHUNKS = TOT_VALUES / CHUNK_SIZE;
	public final static long FILE_INTERVAL = THOUSAND;
	public final static long CPU_CORES = Runtime.getRuntime()
			.availableProcessors();

	/**
	 * 
	 */
	private Constants() {
		// TODO Auto-generated constructor stub
	}

}
