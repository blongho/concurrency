package concurrency.ch04_composing_objects.exercise_4_3;

/**
 *  @author Bernard Che Longho (lobe1602@student.miun.se)
 */
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.*;

/**
 * TODO: The nextValue method returns consecutive values in the range
 * [MIN_VALUE..MAX_VALUE]. When MAX_VALUE is reached the counter is reset to
 * MIN_VALUE and the counting restarts. But when a NumGenerator object is
 * accessed concurrently from multiple threads it does not pass the Unit tests
 * and it also generates exceptions. Apparently we have one or more race
 * conditions. Describe the problems and provide a solution that passes the Unit
 * tests. You may only make changes to the method nextValue and you must not
 * synchronize the whole method.
 *
 * The resetCounter just counts the number of resets of the counter.
 *
 */

//************************ Task a *******************************************
public class NumGenerator {

	static final int MIN_VALUE = -256;
	static final int MAX_VALUE = 255;
	static final int INITIAL_VALUE = MIN_VALUE - 1;

	private final AtomicInteger counter = new AtomicInteger(INITIAL_VALUE);
	private final AtomicInteger resetCounter = new AtomicInteger(0);

	private final Object lock = new Object();

	/**
	 * The original version of nextValue() was not thread save because the compound
	 * action $incrementAndGet() was done without locking.
	 *
	 * There are two ways to fix this, 1. Lock counter while getting the value of
	 * $next 2. Lock the whole procedure (which the instructions forbids) inside the
	 * $lock object.
	 */
	public int nextValue() {
		int next;
		synchronized (counter) {
			next = counter.incrementAndGet();
		}

		if (next > MAX_VALUE) {
			synchronized (lock) {
				int i = counter.get();
				if (i > MAX_VALUE) {
					counter.set(INITIAL_VALUE);
					resetCounter.incrementAndGet();
				}
				next = counter.incrementAndGet();
			}
		}
		return next;
	}
}

 // This too works
//public class NumGenerator {
//
//  static final int MIN_VALUE = -256;
//  static final int MAX_VALUE = 255;
//  static final int INITIAL_VALUE = MIN_VALUE -1;
//
//
//  private final AtomicInteger counter = new AtomicInteger(INITIAL_VALUE);
//  private final AtomicInteger resetCounter = new AtomicInteger(0);
//
//  private final Object lock = new Object();
//
//
//  public int nextValue() {
//  	int next;
//  	synchronized (lock) {
//      next = counter.incrementAndGet();
//      if (next > MAX_VALUE) {
//            int i = counter.get();
//              if (i > MAX_VALUE) {
//                  counter.set(INITIAL_VALUE);
//                  resetCounter.incrementAndGet();
//              }
//              next = counter.incrementAndGet();
//          }
//      }
//      return next;
//  }
//}


// ******************** Task b ************************//
/**
 * According to the documentations,
 * https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html
 * ThreadLocalRandom is guaranteed to give unique random numbers and works well
 * for multithreading. It is supprising to me that this does not pass the test
 *
 */
//public class NumGenerator {
//
//	static final int MIN_VALUE = -256;
//	static final int MAX_VALUE = 255;
//
//	public final int nextValue() {
//		return ThreadLocalRandom.current().nextInt(MIN_VALUE, MAX_VALUE);
//	}
//}
