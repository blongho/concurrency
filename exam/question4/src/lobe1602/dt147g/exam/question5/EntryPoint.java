/**
 * exam lobe1602.dt147g.exam.question5 EntryPoint.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

/**
 * @author longb
 *
 */

enum DisplayState {
	FULL, NOT_FULL
} // State of display at en entry point

class EntryPoint {

	private final Controller controller;
	private final InfoBoard infoBoard = new InfoBoard();
	private final int myEntryId;
	private DisplayState state = DisplayState.NOT_FULL;

	EntryPoint(Controller ctrl, int id) {
		controller = ctrl;
		myEntryId = id;
	}

	// A Car wants to enter the parking, slot>0 returned on success
	/*
	 * If this block is not synchronized, we can potentially hava a problem
	 * of more than one thread accessing this. This might create a "traffic"
	 * jam as controller.hasFreeSlots() might be true for one thread whereas
	 * the other thread sees false.
	 */
	public synchronized int newEntry() {

		int slot = 0; // 0 == no slot free

		if (controller.hasFreeSlots()) {
			slot = controller.requestSlot(myEntryId);
			controller.setSlotOccupied(myEntryId, slot);
		}

		return slot;
	}

	public synchronized void update(int nFree) { // Detect state changes
		if (nFree == 0) {
			state = DisplayState.FULL;
			infoBoard.displayState(myEntryId, state);
		} else if (nFree == 1 && state == DisplayState.FULL) {// Has been full, now not full
			state = DisplayState.NOT_FULL;
			infoBoard.displayState(myEntryId, state);
		}
		// else do nothing

	}

}
