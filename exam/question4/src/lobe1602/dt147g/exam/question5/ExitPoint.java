/**
 * exam lobe1602.dt147g.exam.question5 ExitPoint.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

/**
 * @author longb
 * Make this class thread save by <br>
 * 1. All fields private and final
 * 2. Synchronized access to the class fields
 */

class ExitPoint {
	private final Controller controller;
	private final int myExitId;

	ExitPoint(Controller ctrl, int id) {
		controller = ctrl;
		myExitId = id;
	}

	// A car with slot number slotNo leaves the parking
	/**
	 * We make Controller final so that each thread that accesses ExitPoint
	 * sees a unique Controller object <br>
	 * We also have to synchronize this block to allow for mutual exclusion
	 * 
	 * @param slotNo
	 */
	synchronized void newExit(int slotNo) {
		controller.setSlotFree(myExitId, slotNo);
	}

}
