/**
 * exam lobe1602.dt147g.exam.question5 CarParking.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

import java.util.ArrayList;
import java.util.List;

class CarParking {

	private final Controller controller;
	private final List<Runnable> entries = new ArrayList<>();
	private final List<Runnable> exits = new ArrayList<Runnable>();

	public CarParking() {
		controller = new Controller();
	}

	public void runParking() {
		// entry threads
		for (int id = 0; id < 100; id++) {
			final EntryPoint entryport = new EntryPoint(controller, (id % 4) + 1);

			final ExitPoint exitport = new ExitPoint(controller, (id % 4) + 1);
			Runnable entry = new Runnable() {

				@Override
				public void run() {
					final int freeSlots = entryport.newEntry();
					entryport.update(freeSlots);

				}
			};
			entries.add(entry);

			final int slot = id;
			Runnable exit = new Runnable() {

				@Override
				public void run() {
					exitport.newExit(slot);
				}
			};
			exits.add(exit);
		}

		entries.forEach(entry -> {
			entry.run();
		});

		exits.forEach(exits -> {
			exits.run();
		});
	}

	public static void main(String args[]) {
		final CarParking parking = new CarParking();
		parking.runParking();
	}

}
