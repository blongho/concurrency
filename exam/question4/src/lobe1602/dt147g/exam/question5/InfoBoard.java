/**
 * exam lobe1602.dt147g.exam.question5 InfoBoard.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

/**
 * @author longb
 *
 */

class InfoBoard {

	public synchronized void displayState(int entryId, DisplayState state) {
		switch (state) {
		case FULL:
			System.out.println("Entry " + entryId + " displays FULL");
			break;
		case NOT_FULL:
			System.out.println("Entry " + entryId + " displays WELCOME!");
			break;
		}
	}
}
