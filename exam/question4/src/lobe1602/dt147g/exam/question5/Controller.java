/**
 * exam lobe1602.dt147g.exam.question5 Controller.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

import java.util.ArrayList;

/**
 * @author longb
 *
 */

class Controller {
	private final int NSLOTS = 100;
	private final int NENTRIES = 4;
	private final int NEXITS = 4;
	private final ArrayList<EntryPoint> entries = new ArrayList<>();
	private final ArrayList<ExitPoint> exits = new ArrayList<>();
	private final ArrayList<ParkingSlot> theParkingsSlots = new ArrayList<>();

	private int nFree = NSLOTS;

	public Controller() {
		for (int i = 0; i < NSLOTS; ++i) {
			theParkingsSlots.add(new ParkingSlot(i + 1)); // slots 1..NSLOTS
		}
		for (int i = 0; i < NENTRIES; ++i) {
			entries.add(new EntryPoint(this, i + 1)); // entry points 1..NENTRIES
		}
		for (int i = 0; i < NEXITS; ++i) {
			exits.add(new ExitPoint(this, i + 1)); // exit points 1..NEXITS
		}

	}

	public synchronized boolean hasFreeSlots() {
		return (nFree > 0);
	}

	// Returns id for the chosen parking slot
	public synchronized int requestSlot(int entryId) {
		// Sort the list with respect to distance from entry point given by
		// entryId
		// sort(...);

		for (final ParkingSlot slot : theParkingsSlots) {
			if (slot.getState() == true)
				return slot.getId(); // 1..NSLOT
		}
		return 0; // No free slot
	}

	// Marks a parking slot as occupied
	synchronized void setSlotOccupied(int entryId, int slotId) {

		for (final ParkingSlot ps : theParkingsSlots)
			if (ps.getId() == slotId) {
				ps.setState(false);
				--nFree;
				break;
			}

		System.out.println("Entry id " + entryId + " slot " + slotId + ", " + nFree + " free");
		for (final EntryPoint ep : entries) { // Update displays at the entry points
			ep.update(nFree);
		}

	}

	// Marks a parking slot as free
	public synchronized void setSlotFree(final int exitId, final int slotId) {

		for (final ParkingSlot ps : theParkingsSlots)
			if (ps.getId() == slotId) {
				ps.setState(true);
				++nFree;
				break;
			}

		System.out.println("Exit id " + exitId + " slot " + slotId + ", " + nFree + " free");
		for (final EntryPoint ep : entries) { // Update displays at the entry points
			ep.update(nFree);
		}
	}

	public synchronized final EntryPoint getEntryPoint(int entryId) {
		return entries.get(entryId);
	}

	public synchronized final ExitPoint getExitPoint(int exitId) {
		return exits.get(exitId);
	}

}