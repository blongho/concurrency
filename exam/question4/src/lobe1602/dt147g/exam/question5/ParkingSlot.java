/**
 * exam lobe1602.dt147g.exam.question5 ParkingSlot.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question5;

/**
 * @author longb
 *
 */

class ParkingSlot {
	private final int id;
	private boolean isFree = true;

	public ParkingSlot(int id) {
		this.id = id;
	}

	public synchronized boolean setState(boolean state) {
		isFree = state;
		return isFree;
	}

	public synchronized boolean getState() {
		return isFree;
	}

	public synchronized int getId() {
		return id;
	}

}
