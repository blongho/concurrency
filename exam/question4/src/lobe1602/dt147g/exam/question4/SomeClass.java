/**
 * question1
 * lobe1602.dt147g.exam.question1
 * SomeClass.java
 * 
 * longb
 * Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question4;

/**
 * @author longb
 *
 */
class SomeClass {
	public void doSomething(Info value) {
		System.out.println(value.get());
	}
}
