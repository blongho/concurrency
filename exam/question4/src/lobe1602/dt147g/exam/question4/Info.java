/**
 * question1 lobe1602.dt147g.exam.question1 Info.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question4;

/**
 * @author longb
 *
 */
class Info {
	public Info(int x) {
		xVal = x;
	}

	public synchronized int get() {
		return xVal;
	}

	public synchronized void setX(final int x) {
		xVal = x;

	}

	private int xVal;
}
