/**
 * question1 lobe1602.dt147g.exam.question1 Test.java
 * 
 * longb Jan 19, 2019
 * 
 */
package lobe1602.dt147g.exam.question4;

/**
 * @author longb
 *
 */
class Test {

	public Test(int n) {
		for (int i = 0; i < n; ++i) {
			final Info x = new Info(this.hashCode() + i);
			new Thread() {
				public void run() {
					new SomeClass().doSomething(x);
				}
			}.start();

		}
	}

	//private Info x = null;

	public static void main(String args[]) {
		new Test(10);
	}
}
