package lobe1602.dt147g.exam.question1;

import java.io.*;
import java.util.zip.*;

/**
 * TODO: Fix the concurrency bug Modify this class to allow many threads
 * to call sendMessage() at the same time without any data corruption
 * occuring and for the checksum to still be correct.
 */
public class CheckedSender {
	private final Checksum checksum = new Adler32();

	private final ObjectOutputStream out;
	private static final Object MARKER = new Marker();

	public CheckedSender(final OutputStream out) throws IOException {
		this.out = new ObjectOutputStream(new CheckedOutputStream(out, checksum));
	}

	public synchronized void sendMessage(Object msg) throws IOException {
		// make a local copy of output stream and keep the original output
		// stream uncorrupted
		final ObjectOutputStream outputStream = out;
		outputStream.writeObject(MARKER);
		checksum.reset();
		outputStream.writeObject(msg);
		outputStream.writeLong(checksum.getValue());
		outputStream.reset();
		outputStream.flush();
	}
}
