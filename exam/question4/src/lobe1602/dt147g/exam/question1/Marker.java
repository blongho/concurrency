package lobe1602.dt147g.exam.question1;
import java.io.*;

/**
 * DO NOT CHANGE.
 * <p/>
 * This class is used to avoid checksum errors due to the special marker bytes
 * written out when ObjectOutputStream is created.
 */
public class Marker implements Serializable {
    private static final long serialVersionUID = -4855512266770971540L;
}
