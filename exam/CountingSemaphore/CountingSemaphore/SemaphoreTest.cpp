/**
 * @file	SemaphoreTest.cpp.
 *
 * Implements the semaphore test class
 */

#include "SemaphoreTester.h"
#include <iostream>

/**
 * Main entry-point for this application
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 */

int main() {
	SemaphoreTester tester;
	tester.run();
	return 0; }
