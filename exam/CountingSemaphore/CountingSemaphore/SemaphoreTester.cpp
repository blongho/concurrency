/**
 * @file	SemaphoreTester.cpp.
 *
 * Implements the semaphore tester class
 */

#include "SemaphoreTester.h"
#include <thread>
using std::thread;
#include <iostream>
#include <sstream>
using std::cout;
mutex print;	/* The print */

/** Produces this SemaphoreTester */
void SemaphoreTester::produce()
{
	for (int i = 0; i < 10; i++) {
		sem->signal();
		std::lock_guard<mutex> lock(print);
		cout << "Current count at production: " << sem->getCount() << "\n";
	}
}

/** Consumes this SemaphoreTester */
void SemaphoreTester::consume()
{
	for (int i = 0; i < 10; i++) {
		std::lock_guard<mutex> lock(print);
		cout << "Current count at consumption: " << sem->getCount() << "\n";
		sem->wait();
	}
}

/** Initializes a new instance of the SemaphoreTester class */
SemaphoreTester::SemaphoreTester()
{
	sem = make_shared<Semaphore>(0);
}
/** Finalizes an instance of the SemaphoreTester class */
SemaphoreTester::~SemaphoreTester()
{
}

/** Runs this SemaphoreTester */
void SemaphoreTester::run()
{
	thread pr = thread([&]() { produce();
	});
	thread con = thread([&]() { consume(); });
	if (pr.joinable())
		pr.join();
	if (con.joinable())
		con.join();
}
