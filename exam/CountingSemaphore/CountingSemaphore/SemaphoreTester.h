/**
 * project	:	CountingSemaphore
 * @file   	:	SemaphoreTester.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2019-01-19
 * @summary	:	Declares the semaphore tester class
 *
 *				Copyright (c) 2019 Bernard Che Longho. Permission is hereby granted, free of charge, to any person obtaining.
 **/

#ifndef METAPHORE_TESTER_H
#define METAPHORE_TESTER_H
#include "Semaphore.h"
#include <memory>
using std::shared_ptr;  /* The standard shared pointer */
using std::make_shared; /* The standard make shared */
/** A semaphore tester. */
class SemaphoreTester
{
private:
	shared_ptr<Semaphore> sem;  /* The sem */
	/** Produce calls the sem.signal() 10 times in a thread */
	void produce();
	/** Consumes calls sem.wait() 10 times in another thread */
	void consume();
public:
	/** Initializes a new instance of the SemaphoreTester class */
	SemaphoreTester();
	/** Finalizes an instance of the SemaphoreTester class */
	~SemaphoreTester();
	/** Runs this SemaphoreTester */
	void run();
};

#endif // !METAPHORE_TESTER_H

/**
// End of SemaphoreTester.h
 */

