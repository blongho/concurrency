/**
 * project	:	CountingSemaphore
 * @file   	:	Semaphore.h
 * @author	:	lobe1602(Bernard Che Longho)
 * @since	: 	2019-01-19
 * @summary	:	Declares the semaphore class
 *
 *				Copyright (c) 2019 Bernard Che Longho. Permission is hereby granted, free of charge, to any person obtaining.
 **/

#ifndef SEMAPHORE_H
#define SEMAPHORE_H
#include <condition_variable>
#include <mutex>
using std::mutex;   /* The standard mutex */
using std::condition_variable;  /* The standard condition variable */
/** A semaphore. */
class Semaphore
{
private:
	int count{};	/* Number that will be incremented  */
	mutex mtx;  /* mutex for locking the increment */
	condition_variable cv;  /* The condition variable for notification */

public:

	/**
	 * Initializes a new instance of the Semaphore class
	 *
	 * @param	initialValue	The initial value.
	 */

	Semaphore(int initialValue);
	/** Finalizes an instance of the Semaphore class */
	~Semaphore() = default;
	/** Waits this Semaphore */
	void wait();

	/**
	 * Attempts to wait
	 *
	 * @return	True if it succeeds, false if it fails.
	 */

	bool tryWait();
	/** Signals this Semaphore */
	void signal();

	/**
	 * Gets the count
	 *
	 * @return	The count.
	 */

	int getCount();
};

#endif // !SEMAPHORE_H

/**
// End of Semaphore.h
 */

