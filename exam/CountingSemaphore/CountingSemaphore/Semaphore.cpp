/**
 * @file	Semaphore.cpp.
 *
 * Implements the semaphore class
 */

#include "Semaphore.h"

/**
 * Initializes a new instance of the Semaphore class
 *
 * @param	initialValue	The initial value.
 */

Semaphore::Semaphore(int initialValue){
	std::unique_lock<mutex>lck(mtx);
	count = initialValue;
	lck.unlock();
}
/** Waits this Semaphore */
void Semaphore::wait()
{
	std::unique_lock<mutex> lck(mtx);
	cv.wait(lck, [this]() {return count > 0; });
	count -= 1;
	lck.unlock();
}

/**
 * Attempts to wait
 *
 * @return	True if it succeeds, false if it fails.
 */

bool Semaphore::tryWait()
{
	return mtx.try_lock();
}

/** Signals this Semaphore */
void Semaphore::signal()
{
	std::unique_lock<mutex> lck(mtx);
	count += 1;
	lck.unlock();
	cv.notify_one();
}

/**
 * Gets the count
 *
 * @return	The count.
 */

int Semaphore::getCount()
{
	std::lock_guard<mutex> lock(mtx);
	return count;
}
