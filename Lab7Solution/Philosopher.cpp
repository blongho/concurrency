// File       : Philosopher.cpp
// Description: Implementation of the Philosopher class
// =======================================================================
#include "Philosopher.h"
#include <chrono>
#include <thread>
#include <iostream>

// Constructor
Philosopher::Philosopher(DiningTable* table, std::mutex* writeMutex, std::ofstream& filestream, int plates) 
	: table(table), writeMutex(writeMutex), fileStream(filestream), platesLeft(plates)
{
	// create the generator and distributions
	generator = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
	distribution = std::uniform_int_distribution<int>(1, 5);
	milliDistribution = std::uniform_int_distribution<int>(1, 10);
}

// Seats this philosopher, gets a seatnumber for the table.
void Philosopher::seatMe()
{
	seatNumber = table->seatGuest(); // Get a seat number from the table
	writeMutex->lock(); // lock the write lock

	std::cout << "Philosopher sits down at seat " << seatNumber << "." << " Currently eating: " << table->getCurrentlyEating() <<  std::endl;
	if (fileStream.is_open()) // if filestream is open, write the info to file
	{
		fileStream << "Philosopher sits down at seat " << seatNumber << "." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	}
	writeMutex->unlock(); // unlock write lock
}

// Eat one plate
void Philosopher::eat()
{
	writeMutex->lock(); // lock the write lock
	std::cout << "Philosopher at seat " << seatNumber << " starts eating." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	if (fileStream.is_open()) // if filestream is open, write the info to file
	{
		fileStream << "Philosopher at seat " << seatNumber << " starts eating." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	}
	writeMutex->unlock(); // unlock write lock

	platesLeft--;
	// Eat a random time between 1-5 seconds
	std::this_thread::sleep_for(std::chrono::seconds(distribution(generator)));
}

void Philosopher::sleep()
{
	writeMutex->lock(); // lock the write lock
	std::cout << "Philosopher at seat " << seatNumber << " feels drowsy and sleeps for a while." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	if (fileStream.is_open()) // if filestream is open, write the info to file
	{
		fileStream << "Philosopher at seat " << seatNumber << " feels drowsy and sleeps for a while." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	}
	writeMutex->unlock(); // unlock write lock
	// sleep a random time between 1-5 seconds
	std::this_thread::sleep_for(std::chrono::seconds(distribution(generator)));
}

// Starts the eat/sleep cycle, this is the function that is run by the threads.
void Philosopher::startEating()
{
	while (platesLeft > 0) // while there are plates left to eat
	{
		while (!table->getForks(seatNumber)) // Try to get the forks
		{
			// If getting the forks fails, sleep for a random time between 1-10 ms
			std::this_thread::sleep_for(std::chrono::milliseconds(milliDistribution(generator)));
		}
		// if the philosopher got the forks, eat for a random time between 1-5s
		eat();
		// return the forks
		table->putDownForks(seatNumber);
		// sleep for a random time between 1-5s
		sleep();
	}

	writeMutex->lock(); // lock the write mutex
	std::cout << "Philosopher at seat " << seatNumber << " feels satisfied and leaves the table." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	if (fileStream.is_open()) // if filestream is open, write the info to file
	{
		fileStream << "Philosopher at seat " << seatNumber << " feels satisfied and leaves the table." << " Currently eating: " << table->getCurrentlyEating() << std::endl;
	}
	writeMutex->unlock();
}

