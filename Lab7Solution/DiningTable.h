// File       : DiningTable.h
// Description: Definition for the diningTable class, this is the table that 
//				philosopher sits around.
// =======================================================================
#include <vector>
#include <mutex>
#include <memory>

#ifndef DININGTABLEH
#define DININGTABLEH

class DiningTable {
private:
	std::vector<std::shared_ptr<std::mutex>> forks; // Mutexes that represents the forks
	std::mutex vMutex; // Mutex that restricts the usage of the vector
	int currentlyEating; // Number of philosopher currently eating

public:
	DiningTable() : currentlyEating(0) {};
	int seatGuest(); // Seats a philosopher by the table, returns seat number.
	bool getForks(int seatNr); // Try get the forks closest to seat number
	void putDownForks(int seatNr); // Put down the forks closest to the seat number.
	int getCurrentlyEating() { return currentlyEating; }
};
#endif