// File       : DiningTable.cpp
// Description: implementation of the DiningTable class. 
// =======================================================================
#include "DiningTable.h"


// Adds a fork to the table and returns the seat number to the guest
int DiningTable::seatGuest()
{
	vMutex.lock(); // lock the vector mutex
	// add a new fork to the table
	std::shared_ptr<std::mutex> mutex(new std::mutex());
	forks.push_back(mutex); 
	// Get the seat number
	int seatNr = forks.size() - 1;
	vMutex.unlock(); // unlock the vector mutex
	return seatNr;
}

// Tries to get the forks closest to the seatNr, returns true if successful else false
bool DiningTable::getForks(int seatNr) {
	
	int rightFork = seatNr; // Right fork == the seat number
	int leftFork; 
	vMutex.lock(); // Lock the vector mutex
	
	// if seatNr 0 right fork == the last fork in the vector
	// else right fork == seat number - 1
	if (seatNr == 0)
	{
		leftFork = forks.size() - 1;
	}
	else
	{
		leftFork = seatNr - 1;
	}

	// Try get the right fork
	if (forks[rightFork]->try_lock())
	{
		// Try get the left fork
		if (forks[leftFork]->try_lock()) 
		{
			currentlyEating++; 
			vMutex.unlock(); // unlock the vector mutex
			return true;
		}
		else // if getting the leftfork fails
		{
			forks[rightFork]->unlock(); // return the right fork
			vMutex.unlock(); // unlock the vector mutex
			return false;
		}
	}
	else // if getting the right fork fails
	{
		vMutex.unlock(); // unlock vector mutex
		return false;
	}
}

// Returns the forks closest to the seat number
void DiningTable::putDownForks(int seatNr)
{
	int rightFork = seatNr;
	int leftFork;
	vMutex.lock(); // lock the vector mutex
	// if seatNr 0 right fork == the last fork in the vector
	// else right fork == seat number - 1
	if (seatNr == 0)
	{
		leftFork = forks.size() - 1;
	}
	else
	{
		leftFork = seatNr - 1;
	}
	// return the forks
	forks[leftFork]->unlock();
	forks[rightFork]->unlock();

	currentlyEating--;
	
	vMutex.unlock(); // unlock the vector mutex
}