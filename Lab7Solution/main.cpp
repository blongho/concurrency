// File       : main.cpp
// Description: The main funtion for the dining philosophers application
// =======================================================================
// Description of the solution
// =======================================================================
// Each fork is represented by a mutex.
// When a philosopher tries to eat, he first takes the right fork
// if he succeeds, he also tries to take the left fork, 
// if he fails, he put back both forks and waits for a while and tries again.
// The time they wait to try again is random so that they do not end up in a position 
// where two philosophers constantly try to take the forks at the same time.
// Reading and writing of the mutex Vector is protected by a mutex because it is not thread safe. 
//Also cout and ofstream are protected by a mutex so that no more threads simultaneously try to write to cout or file.
// =======================================================================
#include "DiningTable.h"
#include "Philosopher.h"
#include <string>
#include <vector>
#include <thread>
#include <sstream>
#include <iostream>
#include <fstream>
#include <chrono>
// ===<Functionprototypes>=================================================
bool argumentsProcessor(int argc, char* argv[], int &philosophers, int &plates, std::string &logFile);
// ========================================================================

int main(int argc, char* argv[]) {

	int philosophers = 6; // nr of philosophers
	int plates = 5; // nr of plates to eat for each philosopher
	std::string logFile = "philo.log"; // log filename
	std::mutex writeMutex; // mutex for cout and ofstream

	// Process arguments
	if (!argumentsProcessor(argc, argv, philosophers, plates, logFile))
	{
		return 1;
	}

	DiningTable table;
	std::vector<Philosopher> philos;
	std::vector<std::thread> threads;
	std::ofstream fileStream(logFile);

	// Create and seat each philosopher, save them in a vector so we can start them later
	for (int i = 0; i < philosophers; i++)
	{
		philos.push_back(Philosopher(&table, &writeMutex,fileStream, plates));
		philos[i].seatMe();
	}

	// Start each philosopher in a seperate thread
	for (int i = 0; i < philosophers; i++)
	{
		threads.push_back(std::thread(&Philosopher::startEating, philos[i]));
	}

	// Wait for each philosopher thread
	for (int i = 0; i < threads.size(); i++)
	{
		threads[i].join();
	}

	fileStream.close(); // Close the filestream

	return 0;
}

// processes the arguments
bool argumentsProcessor(int argc, char* argv[], int &philosophers, int &plates, std::string &logFile)
{
	if (argc == 1) // if no arguments, start with default values
	{
		return true;
	}
	else
	{
		for (int i = 1; i < argc; i++) 
		{
			std::string arg = argv[i];

			if (arg == "-n")
			{
				std::istringstream iss(argv[i + 1]);
				if (!(iss >> philosophers))
				{
					std::cerr << "Bad argument: " << argv[i + 1] << std::endl;
					return false;
				}
				if ((philosophers < 4) || (philosophers > 14))
				{
					std::cerr << "Philosophers must be between 4-14" << std::endl;
					return false;
				}

				i++;
			}
			else if (arg == "-d")
			{
				std::istringstream iss(argv[i + 1]);
				if (!(iss >> plates))
				{
					std::cerr << "Bad argument: " << argv[i + 1] << std::endl;
					return false;
				}
				if ((plates < 4) || ( plates > 10))
				{
					std::cerr << "Plates must be between 4-10" << std::endl;
					return false;
				}

				i++;
			}
			else if (arg == "-f")
			{
				std::istringstream iss(argv[i + 1]);
				if (!(iss >> logFile))
				{
					std::cerr << "Bad argument: " << argv[i + 1] << std::endl;
					return false;
				}

				i++;
			}
			else // if unknown switch is found print usage message and quit
			{
				std::cerr << "Unknown switch " << arg << std::endl;
				std::cerr << "Usage: DiningPhilosophers [-n #nr of philosophers] [-d #nr of plates per philosopher] [-f #filename of log file]" << std::endl;
				return false;
			}
		}
	}
	return true;
}