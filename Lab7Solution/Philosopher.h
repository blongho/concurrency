// File       : Philosopher.h
// Description: Definition for the Philosopher class, this class represents
//              each philosopher in the dining philosopher application
// =======================================================================
#include <random>
#include <mutex>
#include <fstream>
#include "DiningTable.h"

#ifndef PHILOSOPHERH
#define PHILOSOPHERH

class Philosopher {

private:
	// Generators and distributions for the eat,sleep and wait times
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution;
	std::uniform_int_distribution<int> milliDistribution;
	
	DiningTable* table; // Pointer to the table
	std::mutex* writeMutex; // Mutex for cout and the ofstream
	std::ofstream& fileStream; // File output stream
	int platesLeft; // Plates left to eat
	int seatNumber = 0; // This philosophers seat number

	void eat();
	void sleep();

public:
	Philosopher(DiningTable* table, std::mutex* writeMutex, std::ofstream& filestream, int plates); // Constructor
	void seatMe(); // Seat the philosopher by the table.
	void startEating(); // Starts the eat/sleep cycle. 

};
#endif